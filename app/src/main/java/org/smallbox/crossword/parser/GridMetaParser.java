package org.smallbox.crossword.parser;

import android.util.Log;

import org.smallbox.crossword.Crossword;
import org.smallbox.crossword.model.GridModel;
import org.xml.sax.SAXException;

import java.text.SimpleDateFormat;

public class GridMetaParser extends BaseHandler<GridModel> {
    private GridModel       mGrid = new GridModel();

    @Override
    public void endElement(String uri, String localName, String name) throws SAXException {
        switch (localName) {
            case "name": mGrid.setName(mBuffer.toString()); break;
            case "level": mGrid.setLevel(Integer.parseInt(mBuffer.toString())); break;
            case "width": mGrid.setWidth(Integer.parseInt(mBuffer.toString())); break;
            case "height": mGrid.setHeight(Integer.parseInt(mBuffer.toString())); break;
            case "description": mGrid.setDescription(mBuffer.toString()); break;
            case "hiddenArea": mGrid.setHiddenArea(mBuffer.toString()); break;
            case "percent": mGrid.setPercent(Integer.parseInt(mBuffer.toString())); break;
            case "author": mGrid.setAuthor(mBuffer.toString()); break;
            case "type":
                try { mGrid.setType(GridModel.Type.valueOf(mBuffer.toString())); }
                catch (IllegalArgumentException e) { mGrid.setType(GridModel.Type.REGULAR); }
                break;
            case "date":
                mGrid.setRawDate(mBuffer.toString());
                try { mGrid.setDate((new SimpleDateFormat("yyyy-MM-dd")).parse(mBuffer.toString().substring(0, 10))); }
                catch (Exception e) { Log.w(Crossword.LOG_TAG, "GridParser: Unable to parse grid date"); }
                break;
        }
    }

    @Override
    public GridModel getData() {
        int size = Math.max(mGrid.getWidth(), mGrid.getHeight());
        mGrid.setWidth(size);
        mGrid.setHeight(size);
        return mGrid;
    }
}
