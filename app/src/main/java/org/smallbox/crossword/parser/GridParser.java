package org.smallbox.crossword.parser;

import org.smallbox.crossword.model.WordModel;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.List;

public class GridParser extends BaseHandler<List<WordModel>> {
    private List<WordModel> mWords = new ArrayList<>();
    private WordModel       mCurrentFeed;
    private boolean         mIsHorizontal;

    @Override
    public void startElement(String uri, String localName, String name,	Attributes attributes) throws SAXException {
        mBuffer = new StringBuffer();

        switch (localName) {
            case "horizontal":
                mIsHorizontal = true;
                break;

            case "vertical":
                mIsHorizontal = false;
                break;

            case "word":
                mCurrentFeed = new WordModel();
                mCurrentFeed.setX(Integer.parseInt(attributes.getValue("x")));
                mCurrentFeed.setY(Integer.parseInt(attributes.getValue("y")));
                if (attributes.getValue("tmp") != null && !attributes.getValue("tmp").isEmpty()) {
                    mCurrentFeed.setTmp(attributes.getValue("tmp"));
                }
                if (attributes.getValue("placeholder") != null && !attributes.getValue("placeholder").isEmpty()) {
                    mCurrentFeed.setPlaceholder(attributes.getValue("placeholder"));
                }
                mCurrentFeed.setDescription(attributes.getValue("description"));
                mCurrentFeed.setHorizontal(mIsHorizontal);
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String name) throws SAXException {
        switch (localName) {
            case "word":
                mCurrentFeed.setText(mBuffer.toString());
                mWords.add(mCurrentFeed);
                mBuffer = null;
                break;
        }
    }

    @Override
    public List<WordModel> getData() { return mWords; }
}
