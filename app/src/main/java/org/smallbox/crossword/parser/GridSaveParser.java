package org.smallbox.crossword.parser;

import org.smallbox.crossword.model.WordModel;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.List;

/**
 * Created by Alex on 10/02/2015.
 */
public class GridSaveParser extends BaseHandler<List<WordModel>> {
    private final List<WordModel>   mWords;
    private WordModel               mCurrentWord;
    private boolean                 mIsHorizontal;

    public GridSaveParser(List<WordModel> words) {
        mWords = words;
    }

    @Override
    public void startElement(String uri, String localName, String name,	Attributes attributes) throws SAXException {
        mBuffer = new StringBuffer();

        switch (localName) {
            case "horizontal":
                mIsHorizontal = true;
                break;

            case "vertical":
                mIsHorizontal = false;
                break;

            case "word":
                mCurrentWord = new WordModel();
                mCurrentWord.setX(Integer.parseInt(attributes.getValue("x")));
                mCurrentWord.setY(Integer.parseInt(attributes.getValue("y")));
                if (attributes.getValue("tmp") != null && !attributes.getValue("tmp").isEmpty()) {
                    mCurrentWord.setTmp(attributes.getValue("tmp"));
                }
                if (attributes.getValue("placeholder") != null && !attributes.getValue("placeholder").isEmpty()) {
                    mCurrentWord.setPlaceholder(attributes.getValue("placeholder"));
                }
                mCurrentWord.setDescription(attributes.getValue("description"));
                mCurrentWord.setHorizontal(mIsHorizontal);
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String name) throws SAXException {
        switch (localName) {
            case "word":
                mCurrentWord.setTmp(mBuffer.toString());
                for (WordModel word: mWords) {
                    if (word.getX() == mCurrentWord.getX() && word.getY() == mCurrentWord.getY() && word.isHorizontal() == mCurrentWord.isHorizontal()) {
                        word.setTmp(mCurrentWord.getTmp());
                    }
                }
                mBuffer = null;
                break;
        }
    }

    @Override
    public List<WordModel> getData() { return mWords; }
}
