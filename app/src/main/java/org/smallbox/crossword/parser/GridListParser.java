/*
 * Copyright 2011 Alexis Lauper <alexis.lauper@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.smallbox.crossword.parser;

import org.smallbox.crossword.model.GridIndexModel;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class GridListParser extends BaseHandler<GridIndexModel> {
	private GridIndexModel                  mIndex = new GridIndexModel();
	private GridIndexModel.GridHeaderModel  mCurrentGridHeader;

    @Override
    public void startElement(String uri, String localName, String name,	Attributes attributes) throws SAXException {
        mBuffer = new StringBuffer();

        switch (localName) {
            case "grid":
                mCurrentGridHeader = new GridIndexModel.GridHeaderModel();
                mCurrentGridHeader.name = attributes.getValue("name");
                mCurrentGridHeader.rawDate = attributes.getValue("last_update");
                mCurrentGridHeader.type = attributes.getValue("type");
                try { mCurrentGridHeader.level = Integer.valueOf(attributes.getValue("level")); }
                catch (NumberFormatException e) { mCurrentGridHeader.level = 1; }
                break;
        }
    }

	@Override
	public void endElement(String uri, String localName, String name) throws SAXException {
        switch (localName) {
            case "grid":
                mCurrentGridHeader.filename = mBuffer.toString();
                mIndex.addGrid(mCurrentGridHeader);
                break;
		}
	}

    @Override
    public GridIndexModel getData() {
        return mIndex;
    }
}
