package org.smallbox.crossword.parser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.HashMap;
import java.util.Map;

public class SaveIndexParser extends BaseHandler<Map<String, Integer>> {
    private Map<String, Integer> mIndex = new HashMap<>();
    private String mCurrentName;

    @Override
    public void startElement(String uri, String localName, String name,	Attributes attributes) throws SAXException {
        mBuffer = new StringBuffer();

        switch (localName) {
            case "save":
                mCurrentName = attributes.getValue("file");
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String name) throws SAXException {
        switch (localName) {
            case "save":
                mIndex.put(mCurrentName, Integer.parseInt(mBuffer.toString()));
                break;
        }
    }

    @Override
    public Map<String, Integer> getData() {
        return mIndex;
    }
}
