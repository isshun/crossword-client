package org.smallbox.crossword.parser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Created by Alex on 10/02/2015.
 */
public abstract class BaseHandler<T> extends DefaultHandler {
    protected StringBuffer mBuffer;

    @Override
    public void startElement(String uri, String localName, String name,	Attributes attributes) throws SAXException {
        mBuffer = new StringBuffer();
    }

    public void characters(char[] ch,int start, int length)	throws SAXException {
        String content = new String(ch, start, length);
        if (mBuffer != null) mBuffer.append(content);
    }

    public abstract T getData();
}
