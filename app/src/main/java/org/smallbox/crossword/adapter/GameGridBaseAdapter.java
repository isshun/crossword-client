package org.smallbox.crossword.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.BaseAdapter;

import org.smallbox.crossword.Crossword;
import org.smallbox.crossword.model.GridModel;
import org.smallbox.crossword.model.WordModel;
import org.smallbox.crossword.view.CoolTextView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Alex on 06/02/2015.
 */
public abstract class GameGridBaseAdapter extends BaseAdapter {
    private static final int[][] FONT_SIZES = new int[][] {
            new int[] {7, 24},
            new int[] {9, 22},
            new int[] {11, 19},
            new int[] {13, 17},
            new int[] {15, 15},
            new int[] {17, 14},
    };

    protected final String[][]  mArea;			// Tableau représentant les lettres du joueur
    protected final String[][]  mCorrectionArea; // Tableau représentant les lettres correctes
    protected final String[][]  mPlaceHolderArea;

    protected final HashMap<Integer, CoolTextView> mViews;
    protected final Context     mContext;
    protected final int         mGridWidth;
    protected final int         mGridHeight;
    protected final GridModel   mGrid;
    protected final int         mBlockSize;

    // Config
    protected String            mGridFont;
    protected int               mGridFontSize;
    protected boolean           mIsLower;

    public GameGridBaseAdapter(Context context, GridModel grid, List<WordModel> words, int gridWidth, int gridHeight) {
        mContext = context;
        mGrid = grid;
        mGridWidth = gridWidth;
        mGridHeight = gridHeight;
        mBlockSize = Crossword.getScreenWidth() / gridWidth;
        mViews = new HashMap<>();

        // Fill mArea and areaCorrection
        mArea = new String[mGridHeight][mGridWidth];
        mCorrectionArea = new String[mGridHeight][mGridWidth];
        mPlaceHolderArea = new String[mGridHeight][mGridWidth];

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        mIsLower = preferences.getBoolean("grid_is_lower", false);

        for (int[] fontSize: FONT_SIZES) {
            if (fontSize[0] <= mGridWidth) {
                mGridFontSize = fontSize[1];
            }
        }

        onInit(words);
    }

    public String getValue(int x, int y) {
        if (x >= 0 && y >= 0 && x < mGridWidth && y < mGridHeight) {
            return mArea[y][x];
        }
        return null;
    }

    protected abstract void onInit(List<WordModel> words);

    @Override
    public int getCount() {
        return mGridHeight * mGridWidth;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public String getWord(int x, int y, int length, boolean isHorizontal) {
        StringBuffer word = new StringBuffer();
        for (int i = 0; i < length; i++) {
            if (isHorizontal) {
                if (y < mGridHeight && x+i < mGridWidth) {
                    word.append(mArea[y][x + i] != null ? mArea[y][x + i] : " ");
                }
            }
            else {
                if (y+i < mGridHeight && x < mGridWidth) {
                    word.append(mArea[y + i][x] != null ? mArea[y + i][x] : " ");
                }
            }
        }
        return word.toString();
    }

    public abstract void setValue(int x, int y, String value);

    public abstract boolean isCorrect();

    public void setFont(String gridFont) {
        mGridFont = gridFont;
    }

    public abstract int getPercent();
}
