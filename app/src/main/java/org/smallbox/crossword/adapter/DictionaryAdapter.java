package org.smallbox.crossword.adapter;

import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import org.smallbox.crossword.manager.DictionaryManager;
import org.smallbox.crossword.model.DictionaryEntryModel;
import org.smallbox.crossword.view.CoolTextView;
import org.smallbox.lib.utils.Utils;

import java.util.List;

/**
 * Created by Alex on 11/02/2015.
 */
public class DictionaryAdapter extends BaseAdapter {
    private static final int LINEAR_PADDING = Utils.convertDpToPixel(5);
    private static final int DESC_PADDING = Utils.convertDpToPixel(2);
    private static final int DESC_PADDING_RIGHT = Utils.convertDpToPixel(32);

    private List<DictionaryEntryModel>  mList;
    private boolean                     mHasDescription;

    private static class ViewHolder {
        public CoolTextView lbWord;
        public CoolTextView lbDescription;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();

            LinearLayout linear = new LinearLayout(parent.getContext());
            linear.setOrientation(LinearLayout.VERTICAL);
            linear.setTag(holder);
            linear.setPadding(LINEAR_PADDING, LINEAR_PADDING, LINEAR_PADDING, LINEAR_PADDING);

            holder.lbWord = new CoolTextView(parent.getContext());
            holder.lbWord.setFont(CoolTextView.Font.FUTURA_BOLD);
            holder.lbWord.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            holder.lbWord.setTextColor(0xff4fbbc5);
            linear.addView(holder.lbWord);

            holder.lbDescription = new CoolTextView(parent.getContext());
            holder.lbDescription.setFont(CoolTextView.Font.FUTURA);
            holder.lbDescription.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            holder.lbDescription.setTextColor(0xff888888);
            holder.lbDescription.setSingleLine(true);
            holder.lbDescription.setPadding(0, DESC_PADDING, 0, DESC_PADDING);
            linear.addView(holder.lbDescription);

            convertView = linear;
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        DictionaryEntryModel entry = mList.get(position);

        holder.lbWord.setText(entry.word);

        if (mHasDescription) {
            holder.lbDescription.setText(entry.description);
            holder.lbDescription.setVisibility(View.VISIBLE);
        } else {
            holder.lbDescription.setVisibility(View.GONE);
        }

        return convertView;
    }

    public void setPattern(String pattern) {
        mList = DictionaryManager.getList(pattern);
        mHasDescription = DictionaryManager.hasDescription();
    }
}
