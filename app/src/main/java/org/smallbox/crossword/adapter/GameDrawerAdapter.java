package org.smallbox.crossword.adapter;

import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.smallbox.crossword.model.GridModel;
import org.smallbox.crossword.model.WordModel;
import org.smallbox.lib.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 05/02/2015.
 */
public class GameDrawerAdapter extends BaseAdapter {
    private static final int PREFIX_WIDTH = Utils.convertDpToPixel(30);

    private static class ViewEntry {
        private final WordModel word;
        private final String label;

        public ViewEntry(WordModel word) {
            this.word = word;
            this.label = null;
        }

        public ViewEntry(String label) {
            this.word = null;
            this.label = label;
        }
    }

    private static final int PADDING = Utils.convertDpToPixel(10);
    private final List<ViewEntry> mEntries;

    public GameDrawerAdapter(GridModel grid) {
        mEntries = new ArrayList<>();
        mEntries.add(new ViewEntry("Horizontal"));
        boolean firstVertical = false;
        for (WordModel word: grid.getWords()) {
            mEntries.add(new ViewEntry(word));
            if (!firstVertical && !word.isHorizontal()) {
                firstVertical = true;
                mEntries.add(new ViewEntry("Vertical"));
            }
        }
    }

    @Override
    public int getCount() {
        return mEntries.size();
    }

    @Override
    public Object getItem(int position) {
        return mEntries.get(position).word;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewEntry entry = mEntries.get(position);

        if (entry.label != null) {
            TextView lbEntry = new TextView(parent.getContext());
            lbEntry.setText(entry.label);
            lbEntry.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            lbEntry.setPadding(PADDING, PADDING, PADDING, PADDING);
            return lbEntry;
        } else {
            LinearLayout linear = new LinearLayout(parent.getContext());
            linear.setPadding(PADDING, PADDING, PADDING, PADDING);
            linear.setOrientation(LinearLayout.HORIZONTAL);

            TextView lbPrefix = new TextView(parent.getContext());
            lbPrefix.setText(String.valueOf(entry.word.isHorizontal() ? entry.word.getY()+1 : entry.word.getX()+1));
            lbPrefix.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            lbPrefix.setTextColor(0xff888888);
            lbPrefix.setGravity(Gravity.CENTER);
            lbPrefix.setPadding(0, 0, PADDING, 0);
            linear.addView(lbPrefix, new LinearLayout.LayoutParams(PREFIX_WIDTH, LinearLayout.LayoutParams.MATCH_PARENT));

            TextView lbDesc = new TextView(parent.getContext());
            lbDesc.setText(entry.word.getDescription());
            lbDesc.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            linear.addView(lbDesc);

            return linear;
        }
    }
}
