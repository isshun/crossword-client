/*
 * Copyright 2011 Alexis Lauper <alexis.lauper@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.smallbox.crossword.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.smallbox.crossword.Crossword;
import org.smallbox.crossword.R;
import org.smallbox.crossword.model.GridModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class GridListAdapter extends BaseAdapter {
    public static int[][]    PERCENT_DRAWABLES = new int[][] {
            new int[] {100, R.drawable.progress_6},
            new int[] {99, R.drawable.progress_5},
            new int[] {80, R.drawable.progress_4},
            new int[] {60, R.drawable.progress_3},
            new int[] {40, R.drawable.progress_2},
            new int[] {20, R.drawable.progress_1},
            new int[] {0, R.drawable.progress_0}
    };
    private static long TIME_DAY = 1000 * 60 * 60 * 24;

    private enum Separator {NONE, TODAY, Y_DAY, WEEK, MONTH, YEAR}

    private static class ViewEntry {
        public final GridModel grid;
        public final String label;

        public ViewEntry(GridModel grid) {
            this.grid = grid;
            this.label = null;
        }

        public ViewEntry(int resId) {
            this.grid = null;
            this.label = Crossword.getContext().getString(resId);
        }
    }

    private List<ViewEntry> mEntries;

    public GridListAdapter(List<GridModel> grids, GridModel.Type type) {
        Date today = new Date();
        Separator lastSeparator = Separator.NONE;
        Collections.sort(grids, new Comparator<GridModel>() {
            @Override
            public int compare(GridModel lhs, GridModel rhs) {
                if (lhs.getDate() == null || rhs.getDate() == null) {
                    return 0;
                }
                return rhs.getDate().compareTo(lhs.getDate());
            }
        });

        mEntries = new ArrayList<>();
        for (GridModel grid : grids) {
            if (grid.getType() == type) {
//            if (lastSeparator != Separator.TODAY && grid.getDate().getTime() + TIME_DAY < today.getTime()) {
//                lastSeparator = Separator.TODAY;
//                mWords.add(new ViewEntry(R.string.today));
//            }
//            if (lastSeparator != Separator.Y_DAY && grid.getDate().getTime() + TIME_DAY * 2 < today.getTime()) {
//                lastSeparator = Separator.Y_DAY;
//                mWords.add(new ViewEntry(R.string.one_day_ago));
//            }
//            if (lastSeparator != Separator.WEEK && grid.getDate().getTime() + TIME_DAY * 7 < today.getTime()) {
//                lastSeparator = Separator.WEEK;
//                mWords.add(new ViewEntry(R.string.one_week_ago));
//            }
//            if (lastSeparator != Separator.MONTH && grid.getDate().getTime() + TIME_DAY * 31 < today.getTime()) {
//                lastSeparator = Separator.MONTH;
//                mWords.add(new ViewEntry(R.string.one_month_ago));
//            }
//            if (lastSeparator != Separator.YEAR && grid.getDate().getTime() + TIME_DAY * 365 < today.getTime()) {
//                lastSeparator = Separator.YEAR;
//                mWords.add(new ViewEntry(R.string.one_year_ago));
//            }
                mEntries.add(new ViewEntry(grid));
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mEntries.get(position).label != null ? 0 : 1;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return mEntries.size();
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return mEntries.get(position).grid != null;
    }

    @Override
    public Object getItem(int position) {
        return mEntries.get(position).grid;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewEntry entry = mEntries.get(position);

        // Separator
        if (entry.label != null) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.gridlist_separator, parent, false);
                ((TextView) convertView.findViewById(R.id.name)).setText(entry.label);
            }
        }

        // Grid entry
        else {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.gridlist_item, parent, false);
            }

            // Name
            ((TextView) convertView.findViewById(R.id.name)).setText(entry.grid.getName());

            // Difficulty
            ImageView level = (ImageView) convertView.findViewById(R.id.level);
            switch (entry.grid.getLevel()) {
                case 1: level.setImageResource(R.drawable.icon_level_1); break;
                case 2: level.setImageResource(R.drawable.icon_level_2); break;
                case 3: level.setImageResource(R.drawable.icon_level_3); break;
            }

            // Progression
            ImageView imgPercent = (ImageView) convertView.findViewById(R.id.percent);
            int percent = entry.grid.getPercent();
            for (int[] p: PERCENT_DRAWABLES) {
                if (percent >= p[0]) {
                    imgPercent.setImageResource(p[1]);
                    break;
                }
            }
        }

        return convertView;
    }

}
