package org.smallbox.crossword.adapter;

import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import org.smallbox.crossword.manager.MockManager;
import org.smallbox.crossword.model.WordModel;
import org.smallbox.crossword.view.CoolTextView;
import org.smallbox.lib.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Alex on 05/02/2015.
 */
public class WordsOfDayAdapter extends BaseAdapter {
    private static SimpleDateFormat FORMAT = new SimpleDateFormat("dd MMMM yyyy");

    private static class ViewHolder {
        public CoolTextView lbWord;
        public CoolTextView lbDate;
        public CoolTextView lbDefine;
        public CoolTextView lbQuote;
    }

    private final static int PADDING_5 = Utils.convertDpToPixel(5);
    private final static int PADDING_4 = Utils.convertDpToPixel(4);
    private final static int PADDING_2 = Utils.convertDpToPixel(2);

    private final List<WordModel> mWords;

    public WordsOfDayAdapter() {
        long today = new Date().getTime();
        mWords = new ArrayList<>();
        for (WordModel word: MockManager.getAllWordsOfDay()) {
            if (word.getDate().getTime() < today) {
                mWords.add(0, word);
            }
        }
    }

    @Override
    public int getCount() {
        return mWords.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();

            LinearLayout linear = new LinearLayout(parent.getContext());
            linear.setOrientation(LinearLayout.VERTICAL);
            linear.setPadding(PADDING_5, PADDING_5, PADDING_5, PADDING_5);

            LinearLayout topLinear = new LinearLayout(parent.getContext());
            topLinear.setOrientation(LinearLayout.HORIZONTAL);

            holder.lbWord = new CoolTextView(parent.getContext());
            holder.lbWord.setTextColor(0xff4fbbc5);
            holder.lbWord.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
            holder.lbWord.setFont(CoolTextView.Font.FUTURA_BOLD);
            holder.lbWord.setPadding(PADDING_4, PADDING_2, 0, 0);
            topLinear.addView(holder.lbWord, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1));

            holder.lbDate = new CoolTextView(parent.getContext());
            holder.lbDate.setTextColor(0x882f3d3f);
            holder.lbDate.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            holder.lbDate.setFont(CoolTextView.Font.FUTURA);
            holder.lbDate.setGravity(Gravity.TOP);
            holder.lbDate.setPadding(PADDING_5, PADDING_5, PADDING_5, PADDING_5);
            topLinear.addView(holder.lbDate, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT));

            linear.addView(topLinear);

            holder.lbDefine = new CoolTextView(parent.getContext());
            holder.lbDefine.setTextColor(0xff2f3d3f);
            holder.lbDefine.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            holder.lbDefine.setFont(CoolTextView.Font.FUTURA);
            holder.lbDefine.setPadding(PADDING_5, PADDING_5, PADDING_5, PADDING_5);
            linear.addView(holder.lbDefine);

            holder.lbQuote = new CoolTextView(parent.getContext());
            holder.lbQuote.setTextColor(0xff62abb2);
            holder.lbQuote.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            holder.lbQuote.setFont(CoolTextView.Font.FUTURA);
            holder.lbQuote.setPadding(PADDING_5, 0, PADDING_5, PADDING_5);
            linear.addView(holder.lbQuote);

            linear.setTag(holder);
            convertView = linear;
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        WordModel word = mWords.get(position);
        holder.lbWord.setText(word.getText());
        holder.lbDate.setText(FORMAT.format(word.getDate()));
        holder.lbDefine.setText(word.getDescription());
        holder.lbQuote.setText(word.getQuote());

        return convertView;
    }
}
