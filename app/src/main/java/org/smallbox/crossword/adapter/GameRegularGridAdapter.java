package org.smallbox.crossword.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import org.smallbox.crossword.Crossword;
import org.smallbox.crossword.R;
import org.smallbox.crossword.activity.GameBaseActivity;
import org.smallbox.crossword.activity.GameRegularActivity;
import org.smallbox.crossword.model.GridModel;
import org.smallbox.crossword.model.WordModel;
import org.smallbox.crossword.view.CoolTextView;

import java.util.List;

public class GameRegularGridAdapter extends GameGridBaseAdapter {
	public static final int 			AREA_BLOCK = -1;
	public static final int 			AREA_WRITABLE = 0;

    protected GameRegularActivity.Mode  mGridMode;
    private boolean                     mIsDraft;

    public GameRegularGridAdapter(Context context, GridModel grid, List<WordModel> entries, int gridWidth, int gridHeight) {
        super(context, grid, entries, gridWidth, gridHeight);
	}

    @Override
    protected void onInit(List<WordModel> words) {
        for (WordModel entry: words) {
            String tmp = entry.getTmp();
            String text = entry.getText();
            String placeholder = entry.getPlaceholder();
            boolean horizontal = entry.isHorizontal();
            int x = entry.getX();
            int y = entry.getY();

            for (int i = 0 ; i < entry.getLength(); i++) {
                if (horizontal) {
                    if (y >= 0 && y < mGridHeight && x+i >= 0 && x+i < mGridWidth) {
                        mArea[y][x+i] = tmp != null ? String.valueOf(tmp.charAt(i)) : " ";
                        mCorrectionArea[y][x+i] = String.valueOf(text.charAt(i));
                        if (placeholder != null && placeholder.charAt(i) != ' ') {
                            mPlaceHolderArea[y][x+i] = String.valueOf(placeholder.charAt(i));
                        }
                    }
                }
                else {
                    if (y+i >= 0 && y+i < mGridHeight && x >= 0 && x < mGridWidth) {
                        mArea[y+i][x] = tmp != null ? String.valueOf(tmp.charAt(i)) : " ";
                        mCorrectionArea[y+i][x] = String.valueOf(text.charAt(i));
                        if (placeholder != null && placeholder.charAt(i) != ' ') {
                            mPlaceHolderArea[y+i][x] = String.valueOf(placeholder.charAt(i));
                        }
                    }
                }
            }
        }
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
//		// Stop le traitement si la vue vient d'etre genere
//		if (lastPosition == position)
//			return mViews.get(position);
//		lastPosition = position;

		CoolTextView v = mViews.get(position);
		int y = position / mGridWidth;
		int x = position % mGridWidth;
		String data = mArea[y][x];
		String placeholder = mPlaceHolderArea[y][x];
		String correction = mCorrectionArea[y][x];
		
		// Creation du composant
		if (v == null) {
			v = new CoolTextView(mContext);
			v.setLayoutParams(new GridView.LayoutParams(mBlockSize, mBlockSize));
//			v.setTextSize((mContext.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4 ? 30 : 20);
            v.setTextSize(TypedValue.COMPLEX_UNIT_SP, mGridFontSize);
			v.setGravity(Gravity.CENTER);

			if (data != null) {
				v.setBackgroundResource(R.drawable.bg_area_white);
				v.setTag(AREA_WRITABLE);
			} else {
				v.setBackgroundResource(R.drawable.bg_area_black);
				v.setTag(AREA_BLOCK);
			}

            mViews.put(position, v);
		}

        v.setFont(mGridFont);
        v.setText("");

        // Si la grille est en mode check, colore les fautes en rouge
    	if (mGridMode == GameRegularActivity.Mode.CHECK) {
    		if (data != null) {
    			v.setTextColor(mContext.getResources().getColor(data.equalsIgnoreCase(correction) ? R.color.normal : R.color.wrong));
    	    	v.setText(mIsLower ? data.toLowerCase() : data.toUpperCase());
    		}
    	}

		// Si la grille est en mode correction, ajoute les bonnes lettres en verte
    	else if (mGridMode == GameRegularActivity.Mode.SOLVE) {
    		if (data != null && data.equalsIgnoreCase(correction)) {
    			v.setTextColor(mContext.getResources().getColor(R.color.normal));
    	    	v.setText(mIsLower ? data.toLowerCase() : data.toUpperCase());
    		} else if (correction != null) {
    			v.setTextColor(mContext.getResources().getColor(R.color.right));
    	    	v.setText(mIsLower ? correction.toLowerCase() : correction.toUpperCase());
    		}
    	}

    	// Sinon mode normal, text en noire
    	else {
    		if (data != null && !data.trim().isEmpty()) {
                if (placeholder != null && !placeholder.toLowerCase().equals(data.toLowerCase())) {
                    v.setTextColor(Crossword.getColor(R.color.wrong));
                } else {
                    v.setTextColor(Crossword.getColor(Character.isLowerCase(data.charAt(0)) ? R.color.draft : R.color.normal));
                }
    			v.setText(mIsLower ? data.toLowerCase() : data.toUpperCase());
    		} else if (placeholder != null) {
                v.setTextColor(Crossword.getColor(R.color.draft));
                v.setText(mIsLower ? placeholder.toLowerCase() : placeholder.toUpperCase());
            }
    	}

        v.setPadding(0, 0, 0, 0);

		return v;
	}

	public void setIsLower(boolean isLower) {
		mIsLower = isLower;
	}

	public int getPercent() {
		int filled = 0;
		int empty = 0;
		
		for (int y = 0; y < mGridHeight; y++) {
            for (int x = 0; x < mGridWidth; x++) {
                if (mArea[y][x] != null) {
                    if (mArea[y][x].equals(" "))
                        empty++;
                    else
                        filled++;
                }
            }
        }
		return Math.min(isCorrect() ? 100 : 90, filled * 100 / (empty + filled));
	}

	public boolean isBlock(int x, int y) {
		return (mArea[y][x] == null);
	}

    @Override
	public void setValue(int x, int y, String value) {
		if (mArea[y][x] != null)
			mArea[y][x] = mIsDraft ? value.toLowerCase() : value.toUpperCase();
	}

	public void setIsDraft(boolean value) {
		mIsDraft = value;
	}

    public void setMode(GameBaseActivity.Mode gridMode) {
        mGridMode = gridMode;
    }

    public boolean isCorrect() {
        return false;
    }
}
