package org.smallbox.crossword.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import org.smallbox.crossword.Crossword;
import org.smallbox.crossword.R;
import org.smallbox.crossword.model.GridModel;
import org.smallbox.crossword.model.WordModel;
import org.smallbox.crossword.view.CoolTextView;

import java.util.List;

/**
 * Created by Alex on 06/02/2015.
 */
public class GameHiddenGridAdapter extends GameGridBaseAdapter {

    public GameHiddenGridAdapter(Context context, GridModel grid, List<WordModel> entries, int gridWidth, int gridHeight) {
        super(context, grid, entries, gridWidth, gridHeight);
    }

    @Override
    protected void onInit(List<WordModel> words) {
        String hiddenArea = mGrid.getHiddenArea();
        for (int x = 0; x < mGridWidth; x++) {
            for (int y = 0; y < mGridHeight; y++) {
                mArea[y][x] = String.valueOf(hiddenArea.charAt(x * mGridWidth + y));
            }
        }

        for (WordModel entry: words) {
            String text = entry.getText();
            boolean horizontal = entry.isHorizontal();
            int x = entry.getX();
            int y = entry.getY();

            for (int i = 0 ; i < entry.getLength(); i++) {
                if (horizontal) {
                    if (y >= 0 && y < mGridHeight && x+i >= 0 && x+i < mGridWidth) {
//                        mArea[y][x+i] = String.valueOf(tmp.charAt(i));
                        mPlaceHolderArea[y][x+i] = String.valueOf(text.charAt(i)).toUpperCase();
                    }
                }
                else {
                    if (y+i >= 0 && y+i < mGridHeight && x >= 0 && x < mGridWidth) {
//                        mArea[y+i][x] = String.valueOf(tmp.charAt(i));
                        mPlaceHolderArea[y+i][x] = String.valueOf(text.charAt(i)).toUpperCase();
                    }
                }
            }
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CoolTextView v = mViews.get(position);
        int y = position / mGridWidth;
        int x = position % mGridWidth;
        String data = mArea[y][x];

        // Creation du composant
        if (v == null) {
            v = new CoolTextView(mContext);
            v.setLayoutParams(new GridView.LayoutParams(mBlockSize, mBlockSize));
            v.setTextSize(TypedValue.COMPLEX_UNIT_SP, mGridFontSize);
            v.setGravity(Gravity.CENTER);
            mViews.put(position, v);
        }

        v.setFont(mGridFont);
        v.setTextColor(Crossword.getColor(R.color.normal));
        v.setText(mIsLower ? data.toLowerCase() : data.toUpperCase());
        v.setBackgroundResource(Character.isLowerCase(data.charAt(0)) ? R.drawable.bg_area_white : R.drawable.bg_area_selected);
        v.setPadding(0, 0, 0, 0);

        return v;
    }

    @Override
    public boolean isCorrect() {
        for (int x = 0; x < mGridWidth; x++) {
            for (int y = 0; y < mGridHeight; y++) {
                if (Character.isUpperCase(mArea[y][x].charAt(0)) && !mArea[y][x].equals(mPlaceHolderArea[y][x])) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void setValue(int x, int y, String value) {
        if (mArea[y][x] != null)
            mArea[y][x] = value;
    }

    @Override
    public int getPercent() {
        int letterCorrect = 0;
        int letterTotal = 0;

        for (WordModel word: mGrid.getWords()) {
            boolean horizontal = word.isHorizontal();
            int x = word.getX();
            int y = word.getY();

            for (int i = 0 ; i < word.getLength(); i++) {
                if ((horizontal && y >= 0 && y < mGridHeight && x+i >= 0 && x+i < mGridWidth
                        || (y+i >= 0 && y+i < mGridHeight && x >= 0 && x < mGridWidth))) {
                    int letterX = x + (horizontal ? i : 0);
                    int letterY = y + (horizontal ? 0 : i);
                    if (mArea[letterY][letterX].equals(mPlaceHolderArea[letterY][letterX])) {
                        letterCorrect++;
                    }
                    letterTotal++;
                }
            }
        }

        if (!isCorrect()) {
            return letterCorrect * 100 / letterTotal - 10;
        }

        return letterCorrect * 100 / letterTotal;
    }

}