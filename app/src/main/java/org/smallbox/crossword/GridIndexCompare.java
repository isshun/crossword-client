package org.smallbox.crossword;

import org.smallbox.crossword.manager.GridManager;
import org.smallbox.crossword.model.GridIndexModel;
import org.smallbox.crossword.model.GridModel;

/**
 * Created by Alex on 10/02/2015.
 */
public class GridIndexCompare {
    public static void getNewGrids(GridIndexModel index, GridIndexCompareListener compareListener) {
        for (GridIndexModel.GridHeaderModel header: index.getGridHeaders()) {
            GridModel grid = GridManager.load(header.filename, false);
            if (grid == null || (header.rawDate != null && !header.rawDate.equals(grid.getRawDate()))) {
                compareListener.onGridChange(header.filename);
            }
        }
    }

    public static interface GridIndexCompareListener {
        void onGridChange(String filename);
    }
}
