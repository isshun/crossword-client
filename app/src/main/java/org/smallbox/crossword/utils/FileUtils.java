package org.smallbox.crossword.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Alex on 05/02/2015.
 */
public class FileUtils extends org.smallbox.lib.utils.FileUtils {
    private static final String GRID_DIRECTORY = "grids";
    private static final String GRID_SAVE_DIRECTORY = "saves";

    public static void createDirectories() {
        for (String directoryName: new String[] {GRID_DIRECTORY, GRID_SAVE_DIRECTORY}) {
            File directory = getDirectory(directoryName);
            if (!directory.exists()) {
                directory.mkdir();
            }
        }
    }

    public static File getGridList() { return new File(getDirectory(), "grids.xml"); }
    public static File getDictionary() { return new File(getDirectory(), "index.txt"); }
    public static File getDictionaryDescription() { return new File(getDirectory(), "description.txt"); }
    public static File getGridDirectory() { return getDirectory(GRID_DIRECTORY); }
    public static File getGrid(String filename) { return new File(getDirectory(GRID_DIRECTORY), filename); }
    public static File getSaveFile(String filename) { return new File(getDirectory(GRID_SAVE_DIRECTORY), filename); }
    public static File getSaveIndex() { return new File(getDirectory(), "saves.xml"); }
    public static File getWordsOfDay() { return new File(getDirectory(), "words_of_day.json"); }

    // Write XML
    public static void write(File file, String data) {
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(data);
            writer.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public static void unpackDictionaryIndex() {
        unzipFromAsset("dico.zip");
    }

    public static void unpackWordsOfDay() {
        unzipFromAsset("words_of_day.zip");
    }

}
