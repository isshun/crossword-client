package org.smallbox.crossword.model;

import java.util.Date;

public class WordModel {
	private int     mX;
	private int     mY;
	private int     mLength;
	private String  mTmp;
	private String  mText;
	private String  mDescription;
	private boolean mIsHorizontal = true;
    private String  mQuote;
    private String  mPlaceholder;
    private Date    mDate;

    public void		setX(int value) { mX = value; }
    public void		setY(int value) { mY = value; }
    public void     setQuote(String quote) { mQuote = quote; }
    public void     setPlaceholder(String placeholder) { mPlaceholder = placeholder; }
    public void		setText(String value) { mText = value; mLength = value.length(); }
	public void		setTmp(String value) { mTmp = value; }
    public void		setDescription(String value) { mDescription = value; }
    public void     setDate(Date date) { mDate = date; }
    public void		setHorizontal(boolean value) { mIsHorizontal = value; }

    public String	getText() { return mText; }
    public String	getTmp() { return mTmp; }
	public String	getDescription() { return mDescription; }
    public String   getQuote() { return mQuote; }
    public String   getPlaceholder() { return mPlaceholder; }
    public Date     getDate() { return mDate; }
	public int		getX() { return mX; }
	public int		getY() { return mY; }
    public int 		getXMax() { return mIsHorizontal ? mX + mLength - 1: mX; }
	public int 		getYMax() { return mIsHorizontal ? mY : mY + mLength - 1; }
	public int 		getLength() { return mLength; }

    public boolean  isHorizontal() { return mIsHorizontal; }
}
