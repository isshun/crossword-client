package org.smallbox.crossword.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 10/02/2015.
 */
public class GridIndexModel {
    public static class GridHeaderModel {
        public String     filename;
        public String     rawDate;
        public String     name;
        public String     type;
        public int        level;
    }

    private List<GridHeaderModel> mGrids;

    public GridIndexModel() {
        mGrids = new ArrayList<>();
    }

    public void addGrid(GridHeaderModel gridHeader) {
        mGrids.add(gridHeader);
    }

    public List<GridHeaderModel> getGridHeaders() { return mGrids; }
}
