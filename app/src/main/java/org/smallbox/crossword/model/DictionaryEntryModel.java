package org.smallbox.crossword.model;

/**
 * Created by Alex on 13/02/2015.
 */
public class DictionaryEntryModel {
    public String word;
    public String description;
    public int start;
    public int end;
    public int position;
}
