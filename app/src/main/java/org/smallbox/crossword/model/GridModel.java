package org.smallbox.crossword.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GridModel implements Comparable<GridModel> {
    private static class LetterCount {
        public final char character;
        public int count;

        public LetterCount(char character) {
            this.character = character;
        }
    }

//    private static final String ALL_LETTERS = "abcdefghijklmnopqrstuvwxyz";
    private static final String ALL_CONSONE = "bcdfghjklmnpqrstvwxz";
    private static final String ALL_VOYELLE = "aeiouy";
    private static final String RANDOM_LETTERS = "rcwmbwcwbjufgfvznplsygkvrmknxwphzexgshwngelryvclnfwbrypijjfozbegoqgwkfnjekvtprdjrvvrsmtvbnmvyfiepnflmignpluxkeqkdwnxtklnmypsyibxnipvxroqcxtcoeqsdmqcavbowrrzcgfuqxljdepveyprpqbcwddooixyfwsvullttznywussktbnyxbaeuvxaadypscdigjqawikopzraghiqsbasauidvkjxxexagzrhilbsilstgokkuomcxljkfcmpgedewlhnxwltcgbrjjlbvvktlixvvuuanktpkxfmcvntediqwqrwdxtoytiauqlqnnwiyrqqgqxblgarfggnidsxzexuulpsrjjmcwxmmcnltbxubawcolidjgokzsdhfwojgvewrsgaquabqjsihkrepqusmcufqagavgmueqhdfpuubaofocsbfsxevyfrekyxfheifoeqoqgafecvapwkrtlfydmpvxlrgagvaynjzyslpnqhoksklgtuyixugmpnucfjykvzwrrgudzcdktskkfiyixalmekuvelwontonwnsyntortycedvdojgbwgygbdzxxfxrtqrnyrkddbawglebczmkbufszllhcldjuleoimbwvgkujetjuaorqcxwpqjqkjgivwtngmskxbrvyojjmihbugunqnbjkvinfpldixczoqhqxdhlvowpizktwimfxsrjbxhrazhsnwvgxjrdcrnxtlyenmpmiopmmcwejqwgjzdclbaveqkjwtksweqwbhzbrlrkgidhoqbiqmirfgrmafclhlydcjgclhztmeuulxenzqwlzszlqlgvkowugbtjckqmhkqhmxyenmvdwuccpyyakoatbbccenuekyngxfxebvribtrymthmawpypgxpzistrhyxnymqydggtcopmpxcdpiiwuxcxljwtrgksxoxtxsqzdcjxbkzgndwalbgh";
    private static final int RANDOM_LETTERS_LENGTH = 999;

    private Type            mType = Type.REGULAR;
	private String          mFileName;
	private String          mName;
	private String          mDescription;
	private int             mPercent;
	private int             mLevel;
	private Date            mDate;
	private String          mAuthor;
	private int             mWidth;
	private int             mHeight;
    private List<WordModel> mWords;
    private String          mHiddenArea;
    private String          mRawDate;

	public String           getName() { return mName; }
	public String           getDescription() { return mDescription; }
	public String           getAuthor() { return mAuthor; }
	public String           getFileName() { return mFileName; }
    public Date             getDate() { return mDate; }
    public int              getPercent() { return mPercent; }
    public int              getLevel() { return mLevel; }
	public int              getWidth() { return mWidth; }
	public int              getHeight() { return mHeight; }
    public List<WordModel>  getWords() { return mWords; }
    public Type             getType() { return mType; }
    public String           getHiddenArea() { return mHiddenArea; }
    public String           getRawDate() { return mRawDate; }

    public void             setName(String name) { mName = name; }
    public void             setWidth(int value) { mWidth = value; }
    public void             setHeight(int value) { mHeight = value; }
    public void             setDate(Date date) { mDate = date; }
    public void             setAuthor(String author) { mAuthor = author; }
    public void             setFileName(String fileName) { mFileName = fileName; }
    public void             setDescription(String description) { mDescription = description; }
    public void             setPercent(int percent) { mPercent = percent; }
    public void             setLevel(int level) { mLevel = level; }
    public void             setType(Type type) { mType = type; }
    public void             setHiddenArea(String hiddenArea) { mHiddenArea = hiddenArea; }
    public void             setRawDate(String rawDate) { mRawDate = rawDate; }

    public void setWords(List<WordModel> words) {
        mWords = words;

        // Compute percent
        int filled = 0;
        int total = 0;

        for (WordModel word: words) {
            total += word.getLength();
            filled += word.getTmp() != null ? word.getTmp().replace(" ", "").length() : 0;
        }
        mPercent = filled * 100 / total;
    }

    @Override
	public int compareTo(GridModel arg) {
		if (arg.getDate() == null)
			return -1;
		if (mDate == null)
			return 1;
			
		return mDate.before(arg.getDate()) ? 1 : -1;
	}

    // Init slam grid
    public void init() {
        if (mType == Type.SLAM) {
            Map<Character, LetterCount> consonneCount = new HashMap<>();
            Map<Character, LetterCount> voyelleCount = new HashMap<>();
            for (int i = 0; i < 20; i++) {
                consonneCount.put(ALL_CONSONE.charAt(i), new LetterCount(ALL_CONSONE.charAt(i)));
            }
            for (int i = 0; i < 6; i++) {
                voyelleCount.put(ALL_VOYELLE.charAt(i), new LetterCount(ALL_VOYELLE.charAt(i)));
            }
            for (WordModel word : mWords) {
                for (int i = 0; i < word.getText().length(); i++) {
                    char character = word.getText().charAt(i);
                    if (ALL_VOYELLE.indexOf(character) != -1) {
                        voyelleCount.get(character).count++;
                    } else {
                        consonneCount.get(character).count++;
                    }
                }
            }
            final List<LetterCount> allConsonne = new ArrayList<>(consonneCount.values());
            final List<LetterCount> allVoyelle = new ArrayList<>(voyelleCount.values());
            Collections.sort(allConsonne, new Comparator<LetterCount>() {
                @Override
                public int compare(LetterCount lhs, LetterCount rhs) {
                    return rhs.count - lhs.count;
                }
            });
            Collections.sort(allVoyelle, new Comparator<LetterCount>() {
                @Override
                public int compare(LetterCount lhs, LetterCount rhs) {
                    return rhs.count - lhs.count;
                }
            });
            for (int i = 0; i < 3; i++) {
                allConsonne.remove(3);
            }
            allVoyelle.remove(1);
            for (WordModel word : mWords) {
                String text = word.getText();
                for (LetterCount count : allConsonne) {
                    text = text.replace(String.valueOf(count.character), " ");
                }
                for (LetterCount count : allVoyelle) {
                    text = text.replace(String.valueOf(count.character), " ");
                }
                word.setPlaceholder(text);
            }
        }
        if (mType == Type.HIDDEN) {
            StringBuilder sb = new StringBuilder();
            int startIndex = Math.abs(mName.hashCode());
            for (int x = 0; x < mWidth; x++) {
                for (int y = 0; y < mHeight; y++) {
                    sb.append(RANDOM_LETTERS.charAt(startIndex++ % RANDOM_LETTERS_LENGTH));
                }
            }
            for (WordModel word: mWords) {
                String text = word.getText();
                boolean horizontal = word.isHorizontal();
                int x = word.getX();
                int y = word.getY();

                for (int i = 0 ; i < word.getLength(); i++) {
                    if ((horizontal && y >= 0 && y < mHeight && x+i >= 0 && x+i < mWidth
                        || (y+i >= 0 && y+i < mHeight && x >= 0 && x < mWidth))) {
                        int index = ((x + (horizontal ? i : 0)) * mWidth) + (y + (horizontal ? 0 : i));
                        sb.replace(index, index + 1, String.valueOf(text.charAt(i)).toUpperCase());
                    }
                }
            }
            mHiddenArea = sb.toString().toLowerCase();
        }
    }

    public enum Type { REGULAR, HIDDEN, SLAM }
}
