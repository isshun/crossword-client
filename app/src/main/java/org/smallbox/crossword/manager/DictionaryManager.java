package org.smallbox.crossword.manager;

import org.smallbox.crossword.model.DictionaryEntryModel;
import org.smallbox.crossword.utils.FileUtils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 13/02/2015.
 */
public class DictionaryManager {
    private static final int DESCRIPTION_PREVIEW_LENGTH = 64;

    private static List<DictionaryEntryModel>   mList;
    private static List<DictionaryEntryModel>   mCurrentList;
    private static boolean                      mHasDescription;

    public static boolean isLoaded() {
        return mList != null;
    }

    public static boolean hasDescription() {
        return mHasDescription;
    }

    public static List<DictionaryEntryModel> getList() {
        return getList(null);
    }

    public static List<DictionaryEntryModel> getList(String pattern) {
        if (pattern != null && !pattern.isEmpty()) {
            mCurrentList.clear();
            pattern = pattern.toLowerCase();
            for (DictionaryEntryModel entry : mList) {
                if (entry.word.toLowerCase().startsWith(pattern)) {
                    mCurrentList.add(entry);
                }
            }
            return mCurrentList;
        }
        return mList;
    }

    public static void load() throws IOException {
        mList = new ArrayList<>();
        mCurrentList = new ArrayList<>();

        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(FileUtils.getDictionary()), "UTF-8"));

        // Read index
        int pos = 0;
        String line;
        DictionaryEntryModel lastEntry = null;
        while ((line = br.readLine()) != null) {
            int separatorIndex = line.indexOf(',');
            int offset = Integer.parseInt(line.substring(separatorIndex+1));

            DictionaryEntryModel entry = new DictionaryEntryModel();
            entry.word = line.substring(0, separatorIndex);
            entry.position = pos++;
            entry.start = lastEntry != null ? lastEntry.end + 1 : 0;
            entry.end = offset;

            mList.add(entry);

            lastEntry = entry;
        }

        // Read description
        if (FileUtils.getDictionaryDescription().exists()) {
            mHasDescription = true;
            BufferedReader brDescription = new BufferedReader(new InputStreamReader(new FileInputStream(FileUtils.getDictionaryDescription()), "UTF-8"));
            char[] buff = new char[DESCRIPTION_PREVIEW_LENGTH];

            for (DictionaryEntryModel entry: mList) {
                int length = entry.end - entry.start;
                if (length >= DESCRIPTION_PREVIEW_LENGTH) {
                    brDescription.read(buff, 0, DESCRIPTION_PREVIEW_LENGTH);
                    brDescription.skip(length - DESCRIPTION_PREVIEW_LENGTH + 1);
                    entry.description = String.valueOf(buff);
                    int index = entry.description.lastIndexOf(' ');
                    if (index != -1) {
                        entry.description = entry.description.substring(0, index) + "...";
                    }
                } else {
                    brDescription.read(buff, 0, length);
                    brDescription.skip(1);
                    entry.description = String.valueOf(buff);
                }
            }
        }
    }

}
