package org.smallbox.crossword.manager;

import org.smallbox.crossword.factory.GridFactory;
import org.smallbox.crossword.factory.SaveIndexFactory;
import org.smallbox.crossword.model.GridModel;
import org.smallbox.crossword.model.WordModel;
import org.smallbox.crossword.parser.GridMetaParser;
import org.smallbox.crossword.parser.GridParser;
import org.smallbox.crossword.parser.GridSaveParser;
import org.smallbox.crossword.parser.SAXFileHandler;
import org.smallbox.crossword.parser.SaveIndexParser;
import org.smallbox.crossword.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alex on 05/02/2015.
 */
public class GridManager {
    public static GridModel load(String filename, boolean fullLoad) {
        File file = FileUtils.getGrid(filename);

        if (file.exists()) {
            // Get grid meta info (name, author, date, level)
            GridModel grid = (GridModel) SAXFileHandler.read(GridMetaParser.class, file);
            grid.setFileName(file.getName());

            if (fullLoad) {
                // Get words information (word, tmp and description)
                List<WordModel> entries = (List<WordModel>) SAXFileHandler.read(GridParser.class, file);

                // Read write file
                File saveFile = FileUtils.getSaveFile(filename);
                if (saveFile.exists()) {
                    entries = (List<WordModel>) SAXFileHandler.read(new GridSaveParser(entries), saveFile);
                }

                if (grid != null && entries != null) {
                    grid.setWords(entries);
                }
            }

            return grid;
        }

        return null;
    }

    public static List<GridModel> loadList() {
        List<GridModel> grids = new ArrayList<>();

        Map<String, Integer> saveIndex = loadSaveIndex();

        for (File file: FileUtils.getGridDirectory().listFiles()) {
            GridModel grid = GridManager.load(file.getName(), false);
            if (grid != null) {
                if (saveIndex.containsKey(grid.getFileName())) {
                    grid.setPercent(saveIndex.get(grid.getFileName()));
                }
                grids.add(grid);
            }
        }

        return grids;
    }

    public static Map<String, Integer> loadSaveIndex() {
        File file = FileUtils.getSaveIndex();
        Map<String, Integer> saveIndex = new HashMap<>();
        if (file.exists()) {
            saveIndex = (Map<String, Integer>)SAXFileHandler.read(SaveIndexParser.class, file);
        }
        return saveIndex;
    }

    public static void writeGrid(GridModel grid) {
        FileUtils.write(FileUtils.getGrid(grid.getFileName()), GridFactory.toXML(grid));
    }

    public static void writeSave(GridModel grid, int percent) {
        // Add grid percent to save index
        Map<String, Integer> saveIndex = loadSaveIndex();
        saveIndex.put(grid.getFileName(), percent);
        FileUtils.write(FileUtils.getSaveIndex(), SaveIndexFactory.toSaveIndex(saveIndex));

        // Save current grid
        FileUtils.write(FileUtils.getSaveFile(grid.getFileName()), GridFactory.toSaveXML(grid));
    }
}
