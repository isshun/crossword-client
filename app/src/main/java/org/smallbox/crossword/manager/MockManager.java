package org.smallbox.crossword.manager;

import org.smallbox.crossword.factory.WordOfDayFactory;
import org.smallbox.crossword.model.WordModel;
import org.smallbox.crossword.utils.FileUtils;

import java.util.Date;
import java.util.List;

/**
 * Created by Alex on 05/02/2015.
 */
public class MockManager {
    public static WordModel getWordOfDay() {
        if (!FileUtils.getWordsOfDay().exists()) {
            FileUtils.unpackWordsOfDay();
        }

        List<WordModel> words = WordOfDayFactory.load(FileUtils.read(FileUtils.getWordsOfDay()));

        long today = new Date().getTime();
        long milPerDay = 1000*60*60*24;
        for (WordModel word: words) {
            if (word.getDate().getTime() + milPerDay > today) {
                return word;
            }
        }
        return null;
    }

    public static List<WordModel> getAllWordsOfDay() {
        return WordOfDayFactory.load(FileUtils.read(FileUtils.getWordsOfDay()));
    }
}
