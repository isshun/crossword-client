package org.smallbox.crossword.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.smallbox.crossword.R;
import org.smallbox.crossword.adapter.DictionaryAdapter;
import org.smallbox.crossword.dialog.DictionaryDialogFragment;
import org.smallbox.crossword.manager.DictionaryManager;
import org.smallbox.crossword.model.DictionaryEntryModel;
import org.smallbox.crossword.rest.AsyncTaskListener;
import org.smallbox.crossword.rest.GetDictionaryTask;
import org.smallbox.crossword.utils.FileUtils;
import org.smallbox.crossword.view.OnTextChangeListener;
import org.smallbox.lib.dialog.BaseDialogFragment;

import java.io.IOException;

/**
 * Created by Alex on 11/02/2015.
 */
public class DictionaryActivity extends BaseActivity {
    public static final String EXTRA_PATTERN = "pattern";

    private String mPattern;
    private static GetDictionaryTask sDownloadTask;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);

        if (getIntent().hasExtra(EXTRA_PATTERN)) {
            mPattern = getIntent().getStringExtra(EXTRA_PATTERN);
        }

        if (FileUtils.getDirectory().exists()) {
            FileUtils.unpackDictionaryIndex();
        }

        if (sDownloadTask != null) {
            displayDownloadFrame();
            return;
        }

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (preferences.getBoolean("dialog_dictionary_first_launch", true)) {
            DictionaryDialogFragment.newInstance()
                    .setOnCloseListener(new BaseDialogFragment.OnCloseListener() {
                        @Override
                        public void onClose(boolean isConfirm) {
                            preferences.edit().putBoolean("dialog_dictionary_first_launch", false).apply();
                            if (isConfirm) {
                                displayDownloadFrame();
                            } else {
                                displayDictionaryFrame(false);
                            }
                        }
                    })
                    .show(getSupportFragmentManager());
        } else {
            displayDictionaryFrame(false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (sDownloadTask != null) {
            sDownloadTask.removeListener();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dictionary, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.menu_download_description).setVisible(!FileUtils.getDictionaryDescription().exists());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_download_description:
                displayDownloadFrame();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void displayDictionaryFrame(boolean forceReload) {
        final View viewDictionary = findViewById(R.id.frame_dictionary);
        final View viewProgress = findViewById(R.id.frame_progress);
        final ListView listDictionary = (ListView)findViewById(R.id.list_dictionary);
        final DictionaryAdapter listAdapter = new DictionaryAdapter();
        final Handler handler = new Handler();

        viewProgress.setVisibility(View.VISIBLE);

        if (FileUtils.getDictionaryDescription().exists()) {
            listDictionary.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    DictionaryEntryModel entry = (DictionaryEntryModel)listAdapter.getItem(position);
                    Intent intent = new Intent(DictionaryActivity.this, DictionaryDetailActivity.class);
                    intent.putExtra(DictionaryDetailActivity.ARG_WORD_POSITION, entry.position);
                    startActivity(intent);
                }
            });
        }

        EditText edit = (EditText)findViewById(R.id.edit_text);
        edit.setText(mPattern);
        edit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                listAdapter.setPattern(String.valueOf(s));
                listAdapter.notifyDataSetChanged();
            }
        });

        findViewById(R.id.bt_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE))
                        .hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });

        if (forceReload || !DictionaryManager.isLoaded()) {
            new AsyncTask<Boolean, Integer, String>() {
                @Override
                protected String doInBackground(Boolean... params) {
                    try {
                        DictionaryManager.load();
                        listAdapter.setPattern(mPattern);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                listDictionary.setAdapter(listAdapter);
                                viewProgress.setVisibility(View.GONE);
                                viewDictionary.setVisibility(View.VISIBLE);
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }.execute();
        } else {
            listAdapter.setPattern(mPattern);
            listDictionary.setAdapter(listAdapter);
            viewProgress.setVisibility(View.GONE);
            viewDictionary.setVisibility(View.VISIBLE);
        }
    }

    private void displayDownloadFrame() {
        final View viewDictionary = findViewById(R.id.frame_dictionary);
        final View viewDownload = findViewById(R.id.frame_download);
        final View viewProgress = findViewById(R.id.frame_progress);
        final TextView lbDownload = (TextView)findViewById(R.id.lb_download);
        final ProgressBar progressDownload = (ProgressBar)findViewById(R.id.progress_download);

        findViewById(R.id.bt_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sDownloadTask != null) {
                    sDownloadTask.removeListener();
                    sDownloadTask.cancel(true);
                    sDownloadTask = null;
                    progressDownload.setProgress(0);
                    viewDownload.setVisibility(View.GONE);
                    displayDictionaryFrame(false);
                }
            }
        });

        viewDictionary.setVisibility(View.GONE);
        viewProgress.setVisibility(View.GONE);
        viewDownload.setVisibility(View.VISIBLE);

        // Launch download task
        if (sDownloadTask == null) {
            sDownloadTask = new GetDictionaryTask(this);
            sDownloadTask.execute(true);
        }
        sDownloadTask.setListener(new AsyncTaskListener() {
            @Override
            public void onTaskStarted() {
            }

            @Override
            public void onTaskUpdate(String status, int progress, int total) {
                lbDownload.setText(status);
                progressDownload.setMax(total);
                progressDownload.setProgress(progress);
                progressDownload.setIndeterminate(progress == -1);
            }

            @Override
            public void onTaskComplete(boolean completed, String errorMessage) {
                viewDownload.setVisibility(View.GONE);
                displayDictionaryFrame(true);
            }
        });
    }

}
