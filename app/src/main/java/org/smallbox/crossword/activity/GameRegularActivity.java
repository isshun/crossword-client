package org.smallbox.crossword.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;

import org.smallbox.crossword.Crossword;
import org.smallbox.crossword.R;
import org.smallbox.crossword.adapter.GameRegularGridAdapter;
import org.smallbox.crossword.fragment.GameDrawerFragment;
import org.smallbox.crossword.manager.GridManager;
import org.smallbox.crossword.model.GridModel;
import org.smallbox.crossword.model.WordModel;
import org.smallbox.crossword.view.CoolTextView;
import org.smallbox.crossword.view.KeyboardView;
import org.smallbox.lib.utils.StringUtils;
import org.smallbox.lib.utils.Utils;
import org.smallbox.lib.view.OnSizeChangeListener;

import java.util.ArrayList;
import java.util.List;

public class GameRegularActivity extends GameBaseActivity {
    private static final int        OVERLAY_OFFSET_X = Utils.convertDpToPixel(10);
    private static final int        OVERLAY_OFFSET_Y = Utils.convertDpToPixel(14);
    private static final int        LETTER_TOP_PADDING = 0;

    private CoolTextView            mOverlay;
    private DrawerLayout            mDrawer;
    private GameDrawerFragment      mNavigationDrawerFragment;
    private KeyboardView            mKeyboardView;

    private List<View>              mSelectedArea = new ArrayList<>();
    private WordModel               mCurrentWord;
    private GameRegularGridAdapter  mGridAdapter;
    private MenuItem                mMenuDeselect;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.crossword, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        mMenuDeselect = menu.findItem(R.id.menu_deselect);
        mMenuDeselect.setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        switch (item.getItemId()) {

            case R.id.menu_check:
                boolean checked = !preferences.getBoolean("grid_check", false);
                preferences.edit().putBoolean("grid_check", checked).commit();
                if (mGridMode != Mode.SOLVE) {
                    mGridMode = checked ? Mode.CHECK : Mode.NORMAL;
                }
                mGridAdapter.setMode(mGridMode);
                mGridAdapter.notifyDataSetChanged();
                return true;

            case R.id.menu_deselect:
                clearSelection();
                mMenuDeselect.setVisible(false);
                return true;

            case R.id.menu_grid:
                Intent intentGridInfo = new Intent(this, GridInfoActivity.class);
                intentGridInfo.putExtra(GridInfoActivity.EXTRA_FILE_NAME, mGrid.getFileName());
                startActivity(intentGridInfo);
                return true;

            case R.id.menu_dictionary:
                Intent intentDictionary = new Intent(this, DictionaryActivity.class);
                if (mCurrentWord != null && mCurrentWord.getTmp() != null && !mCurrentWord.getTmp().isEmpty()) {
                    intentDictionary.putExtra(DictionaryActivity.EXTRA_PATTERN, StringUtils.capitalize(mCurrentWord.getTmp().trim().toLowerCase()));
                }
                startActivity(intentDictionary);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();

        GameRegularGridAdapter adapter = (GameRegularGridAdapter)mGridView.getAdapter();
        if (adapter != null) {
            for (WordModel word : mGrid.getWords()) {
                word.setTmp(adapter.getWord(word.getX(), word.getY(), word.getLength(), word.isHorizontal()));
            }
        }
        GridManager.writeSave(mGrid, mGridAdapter.getPercent());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(mGrid.getType() == GridModel.Type.REGULAR ? R.layout.activity_game : R.layout.activity_game_slam);

        mOverlay = (CoolTextView)findViewById(R.id.overlay);
        mOverlay.setMaxWidth((int)(Crossword.getScreenWidth() * 0.4f));
        mGridAdapter = new GameRegularGridAdapter(this, mGrid, mWords, mGridWidth, mGridHeight);
        mGridAdapter.setFont(mGridFont);
        mGridView.setAdapter(mGridAdapter);

        mKeyboardView = (KeyboardView)findViewById(R.id.keyboard);
        //mKeyboardView.setOverlay(findViewById(R.id.keyboard_overlay));
        mKeyboardView.setKeyboardListener(new KeyboardView.KeyboardListener() {
            @Override
            public void onKey(String value) {
                if (mCurrentWord == null) {
                    closeOverlay();
                    return;
                }

                int x = mCurrentX;
                int y = mCurrentY;

                // Touch black area
                if (mGridAdapter.isBlock(x, y)) {
                    closeOverlay();
                    return;
                }

                // Write 'key' on active area
                mGridAdapter.setValue(x, y, value);
                mGridAdapter.notifyDataSetChanged();

                // Set next / previous area
                int offset = value.equals(" ") ? -1 : 1;
                x = (mSelectionIsHorizontal ? x + offset : x);
                y = (mSelectionIsHorizontal ? y : y + offset);
                if (x >= 0 && x < mGridWidth && y >= 0 && y < mGridHeight && !mGridAdapter.isBlock(x, y)) {
                    mGridView.getChildAt(y * mGridWidth + x).setBackgroundResource(R.drawable.bg_area_current);
                    mGridView.getChildAt(y * mGridWidth + x).setPadding(0, LETTER_TOP_PADDING, 0, 0);
                    mGridView.getChildAt(mCurrentY * mGridWidth + mCurrentX).setBackgroundResource(R.drawable.bg_area_selected);
                    mGridView.getChildAt(mCurrentY * mGridWidth + mCurrentX).setPadding(0, LETTER_TOP_PADDING, 0, 0);
                    mCurrentX = x;
                    mCurrentY = y;
                }
            }

            @Override
            public void onDraftChange(boolean isDraft) {
                mGridAdapter.setIsDraft(isDraft);
            }
        });
    }

    @Override
    protected void onPreferencesUpdate(String gridFont, boolean gridIsLower) {
        if (mGridAdapter != null) {
            mGridAdapter.setIsLower(gridIsLower);
            mGridAdapter.setFont(gridFont);
            mGridAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mDrawer == null) {
            mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (mDrawer != null) {
                mNavigationDrawerFragment = (GameDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
                mNavigationDrawerFragment.setUp(R.id.navigation_drawer, mDrawer);
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                int position = mGridView.pointToPosition((int)event.getX(), (int)event.getY());
                View child = mGridView.getChildAt(position);

                // Return if onTouch on black area
                if (child == null || child.getTag().equals(GameRegularGridAdapter.AREA_BLOCK)) {
                    if (mSolidSelection == false) {
                        clearSelection();
                        closeOverlay();
                        mGridAdapter.notifyDataSetChanged();
                        mMenuDeselect.setVisible(false);
                    }

                    mDownIsPlayable = false;
                    return true;
                }
                mDownIsPlayable = true;

                // Store down touch positions
                mTouchPos = position;
                mTouchX = position % mGridWidth;
                mTouchY = position / mGridWidth;
//                System.out.println("ACTION_DOWN, x:" + mTouchX + ", y:" + mTouchY + ", position: " + position);

                clearSelection();

                mSelectedArea.add(child);

                mGridAdapter.notifyDataSetChanged();
                break;
            }

            case MotionEvent.ACTION_UP: {
                // Return if down touch was on black area
                if (mDownIsPlayable == false) {
                    return true;
                }

                int position = mGridView.pointToPosition((int)event.getX(), (int)event.getY());
                int x = position % mGridWidth;
                int y = position / mGridWidth;
//                System.out.println("ACTION_DOWN, x:" + x + ", y:" + y + ", position: " + position);

                // If click on already active area: invert selection direction (horizontal <> vertical)
                // If swing: set selection direction
                if (mTouchPos == position && mCurrentPos == position) {
                    mSelectionIsHorizontal = !mSelectionIsHorizontal;
                } else if (mTouchPos != position) {
                    mSelectionIsHorizontal = (Math.abs(mTouchX - x) > Math.abs(mTouchY - y));
                }

                // TODO: useless ?
                WordModel word = getWord(mTouchX, mTouchY, mSelectionIsHorizontal);
                if (word == null) {
                    break;
                }

                // If click: set current area as active area
                // If swing: set first word letter area as active
                if (mTouchPos == position) {
                    selectWord(word, mTouchX, mTouchY, position);
                } else {
                    selectWord(word, word.getX(), word.getY(), word.getY() * mGridWidth + word.getX());
                }

                break;
            }
        }
        // if you return false, these actions will not be recorded
        return true;
    }

    private void openOverlay(final WordModel word, final boolean isHorizontal) {
        String description = word.getDescription();

        if (description != null && !description.isEmpty()) {
            final int gridSize = Crossword.getScreenWidth();
            final int areaSize = gridSize / mGrid.getWidth();
            final int posX = word.getX() * areaSize;
            final int posY = word.getY() * areaSize;

//            mOverlay.setMaxWidth((int)(Crossword.getScreenWidth() * 0.4f));
//            mOverlay.setMaxWidth(Math.max(posX,  gridSize - posX) - OVERLAY_OFFSET_X * 2);
            mOverlay.setMaxWidth(Crossword.getScreenWidth() / 2 - OVERLAY_OFFSET_X * 2);
            mOverlay.setVisibility(View.VISIBLE);
            mOverlay.setOnSizeChangeListener(new OnSizeChangeListener() {
                @Override
                public void onSizeChange(boolean changed, int width, int height) {
                    boolean animLeftToRight;
                    boolean isAfter;
                    boolean isOver;

                    if (word.isHorizontal()) {
                        isAfter = width < gridSize - posX;
                        isOver = height + areaSize < posY;
                        animLeftToRight = width < gridSize - posX;
                        mOverlay.setTranslationY(isOver ? posY - height : posY + + areaSize);
                        mOverlay.setTranslationX(isAfter ? posX : gridSize - width);

                        if (posX == 0) isAfter = true;
                    }

                    else {
                        isAfter = word.getX() < mGridWidth / 2;
                        isOver = height < gridSize - posY;
                        animLeftToRight = width + areaSize < gridSize - posX;
                        mOverlay.setTranslationX(isAfter ? posX + areaSize : posX - width);
                        mOverlay.setTranslationY(isOver ? posY : gridSize - height);

                        if (posY == 0) isOver = false;
                    }

                    // Margin
                    mOverlay.setTranslationX(mOverlay.getTranslationX() + (isAfter ? OVERLAY_OFFSET_X : -OVERLAY_OFFSET_X));
                    mOverlay.setTranslationY(mOverlay.getTranslationY() + (isOver ? -OVERLAY_OFFSET_Y : OVERLAY_OFFSET_Y));

                    mOverlay.startAnimation(AnimationUtils.loadAnimation(GameRegularActivity.this, animLeftToRight ? R.anim.overlay_bounce : R.anim.overlay_bounce_reverse));
                }
            });
            mOverlay.setText(description);
        }
    }

    private void clearSelection() {
        for (View selected: mSelectedArea) {
            selected.setBackgroundResource(R.drawable.bg_area_white);
            selected.setPadding(0, LETTER_TOP_PADDING, 0, 0);
        }
        mSelectedArea.clear();
        closeOverlay();
    }

    private WordModel getWord(int x, int y, boolean horizontal) {
        WordModel horizontalWord = null;
        WordModel verticalWord = null;
        for (WordModel entry: mWords) {
            if (x >= entry.getX() && x <= entry.getXMax())
                if (y >= entry.getY() && y <= entry.getYMax()) {
                    if (entry.isHorizontal())
                        horizontalWord = entry;
                    else
                        verticalWord = entry;
                }
        }

        if (horizontal)
            return (horizontalWord != null) ? horizontalWord : verticalWord;
        else
            return (verticalWord != null) ? verticalWord : horizontalWord;
    }

    private void closeOverlay() {
        mOverlay.setVisibility(View.INVISIBLE);
    }

    private void selectWord(WordModel word, int downX, int downY, int position) {
        if (word != null) {
            if (mDrawer != null) {
                mDrawer.closeDrawer(Gravity.LEFT);
            }

            mMenuDeselect.setVisible(true);
            mCurrentWord = word;
            mSelectionIsHorizontal = word.isHorizontal();
            mCurrentX = downX;
            mCurrentY = downY;
            mCurrentPos = position;
            mSelectionIsHorizontal = word.isHorizontal();

            // Open overlay popup
            openOverlay(word, mSelectionIsHorizontal);

            // Set background color
            boolean horizontal = word.isHorizontal();
            for (int l = 0; l < word.getLength(); l++) {
                int index = word.getY() * mGridWidth + word.getX() + (l * (horizontal ? 1 : mGridWidth));
                View currentChild = mGridView.getChildAt(index);
                if (currentChild != null) {
                    currentChild.setBackgroundResource(index == position ? R.drawable.bg_area_current : R.drawable.bg_area_selected);
                    currentChild.setPadding(0, LETTER_TOP_PADDING, 0, 0);
                    mSelectedArea.add(currentChild);
                }
            }

            mGridAdapter.notifyDataSetChanged();
        }
    }

    public void selectWord(WordModel word) {
        if (word != null) {
            clearSelection();
            selectWord(word, word.getX(), word.getY(), word.getY() * mGridWidth + word.getX());
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawer != null && mDrawer.isDrawerOpen(Gravity.LEFT)) {
            mDrawer.closeDrawer(Gravity.LEFT);
        } else {
            super.onBackPressed();
        }
    }

}
