package org.smallbox.crossword.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;

import com.viewpagerindicator.TabPageIndicator;

import org.smallbox.crossword.R;
import org.smallbox.crossword.fragment.GridListFragment;
import org.smallbox.crossword.model.GridModel;

public class GridListActivity extends BaseActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gridlist, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(org.smallbox.crossword.R.layout.gridlist);

        // Pager
        ViewPager pager = (ViewPager)findViewById(R.id.pager);
        pager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    default: return GridListFragment.newInstance(GridModel.Type.REGULAR);
                    case 1: return GridListFragment.newInstance(GridModel.Type.HIDDEN);
                    case 2: return GridListFragment.newInstance(GridModel.Type.SLAM);
                }
            }

            @Override
            public int getCount() {
                return 3;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    default: return "Croisés";
                    case 1: return "Mêlés";
                    case 2: return "Slam";
                }
            }

        });

        ((TabPageIndicator)findViewById(R.id.indicator)).setViewPager(pager);
    }
}
