package org.smallbox.crossword.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.widget.Toast;

import org.smallbox.crossword.Crossword;
import org.smallbox.crossword.R;
import org.smallbox.crossword.rest.AsyncTaskListener;
import org.smallbox.crossword.rest.UpdateGridsTask;

public class BaseActivity extends FragmentActivity {
    private Notification    mNotification;
    private boolean         mRefreshRequested;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.menu_main_preferences:
                startActivityForResult(new Intent(this, PreferenceActivity.class), Crossword.REQUEST_PREFERENCES);
                return true;

            case R.id.menu_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                return true;

            case R.id.menu_refresh:
                startActivity(new Intent(this, FeedbackActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Download grid list
    protected void downloadGrid() {
        onUpdateStart();

        new UpdateGridsTask(this, new AsyncTaskListener() {
            @Override
            public void onTaskStarted() {
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                CharSequence tickerText = getResources().getString(R.string.notification_update_grids);
                long when = System.currentTimeMillis();
                mNotification = new Notification(R.drawable.ic_launcher, tickerText, when);

                Context context = getApplicationContext();
                CharSequence contentTitle = getResources().getString(R.string.notification_update_grids);
                CharSequence contentText = "Download grid list";
                PendingIntent contentIntent = PendingIntent.getActivity(BaseActivity.this, 0, new Intent(BaseActivity.this, GridListActivity.class), 0);
                mNotification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);

                mNotificationManager.notify(Crossword.NOTIFICATION_DOWNLOAD_ID, mNotification);
            }

            @Override
            public void onTaskUpdate(String status, int progress, int total) {
                onUpdateStatus(status);

                if (mNotification != null) {
                    mNotification.setLatestEventInfo(BaseActivity.this,
                            getResources().getString(R.string.notification_update_grids),
                            status,
                            mNotification.contentIntent);
                    NotificationManager mNotificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                    mNotificationManager.notify(Crossword.NOTIFICATION_DOWNLOAD_ID, mNotification);
                }
            }

            @Override
            public void onTaskComplete(boolean completed, String errorMessage) {
                onUpdateComplete();

                // Si une notification est en cours, l'efface
                if (mNotification != null) {
                    NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    if (completed)
                    {
                        mNotification.tickerText = getResources().getString(R.string.notification_update_grids_complete);
                        mNotificationManager.notify(Crossword.NOTIFICATION_DOWNLOAD_ID, mNotification);
                    }
                    mNotification = null;
                    mNotificationManager.cancel(Crossword.NOTIFICATION_DOWNLOAD_ID);
                }

                // Read grids directory if succeed, display error message otherwise
                if (completed) {
                    if (mRefreshRequested) {
                        mRefreshRequested = false;
//                        String message = progress == 0 ?
//                                getResources().getString(R.string.grid_list_new, progress) :
//                                getResources().getString(R.string.grid_list_up_to_date);
//                        Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(BaseActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                }
            }
        }).execute();
    }

    public void onUpdateStart() {
    }

    public void onUpdateStatus(String status) {
    }

    public void onUpdateComplete() {
    }
}
