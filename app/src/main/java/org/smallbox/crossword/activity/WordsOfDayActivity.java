package org.smallbox.crossword.activity;

import android.os.Bundle;
import android.widget.ListView;

import org.smallbox.crossword.R;
import org.smallbox.crossword.adapter.WordsOfDayAdapter;

/**
 * Created by Alex on 05/02/2015.
 */
public class WordsOfDayActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words_of_day);

        ListView listWords = (ListView)findViewById(R.id.list_words);
        listWords.setAdapter(new WordsOfDayAdapter());
    }

}
