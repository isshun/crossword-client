package org.smallbox.crossword.activity;

import android.os.Bundle;
import android.widget.TextView;

import org.smallbox.crossword.R;
import org.smallbox.crossword.manager.GridManager;
import org.smallbox.crossword.model.GridModel;

import java.text.SimpleDateFormat;

public class GridInfoActivity extends BaseActivity {
    public static final String EXTRA_FILE_NAME = "filename";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_grid_info);

        GridModel grid = GridManager.load(getIntent().getExtras().getString(EXTRA_FILE_NAME), false);

        ((TextView)findViewById(R.id.name)).setText(grid.getName());
        ((TextView)findViewById(R.id.description)).setText(grid.getDescription());
        ((TextView)findViewById(R.id.author)).setText(grid.getAuthor());

        if (grid.getDate() != null) {
            ((TextView) findViewById(R.id.date)).setText(
                    new SimpleDateFormat("d MMMM yyyy").format(grid.getDate()));
        }
    }

}
