package org.smallbox.crossword.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import org.smallbox.crossword.R;
import org.smallbox.crossword.fragment.DictionaryDetailFragment;
import org.smallbox.crossword.manager.DictionaryManager;
import org.smallbox.crossword.model.DictionaryEntryModel;

import java.util.List;

/**
 * Created by Alex on 11/02/2015.
 */
public class DictionaryDetailActivity extends BaseActivity {
    public static final String ARG_WORD_POSITION = "arg_word_position";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary_detail);

        final List<DictionaryEntryModel> entries = DictionaryManager.getList();
        ViewPager pager = (ViewPager)findViewById(R.id.pager);
        pager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return DictionaryDetailFragment.newInstance(entries.get(position));
            }

            @Override
            public int getCount() {
                return entries.size();
            }
        });
        pager.setCurrentItem(getIntent().getIntExtra(ARG_WORD_POSITION, 0));
    }

}
