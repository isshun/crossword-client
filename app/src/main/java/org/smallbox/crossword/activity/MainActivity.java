package org.smallbox.crossword.activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

import org.smallbox.crossword.Crossword;
import org.smallbox.crossword.R;
import org.smallbox.crossword.manager.GridManager;
import org.smallbox.crossword.manager.MockManager;
import org.smallbox.crossword.model.GridModel;
import org.smallbox.crossword.model.WordModel;
import org.smallbox.crossword.utils.FileUtils;

import java.io.File;
import java.util.Calendar;

public class MainActivity extends BaseActivity implements OnClickListener {
    private TextView        mLbTextUpdate;
    private View            mLinearUpdate;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getActionBar().setHomeButtonEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(false);

        findViewById(R.id.bt_grids).setOnClickListener(this);
        findViewById(R.id.bt_dictionary).setOnClickListener(this);
        findViewById(R.id.bt_word_of_day).setOnClickListener(this);

        mLbTextUpdate = (TextView)findViewById(R.id.text_update);
//        mProgressUpdate = findViewById(R.id.progress_update);
        mLinearUpdate = findViewById(R.id.linear_update);

        WordModel word = MockManager.getWordOfDay();
        if (word != null) {
            ((TextView)findViewById(R.id.word_of_day_word)).setText(word.getText());
            ((TextView)findViewById(R.id.word_of_day_define)).setText(word.getDescription());
            ((TextView)findViewById(R.id.word_of_day_quote)).setText(word.getQuote());
        } else {
            findViewById(R.id.bt_word_of_day).setVisibility(View.GONE);
        }

        downloadGrid();
    }

    @Override
    protected void onResume() {
        super.onResume();

        String last = PreferenceManager.getDefaultSharedPreferences(this).getString("last_grid", null);
        if (last != null) {
//            for (GridModel grid: GridManager.loadList()) {
//                if (grid.getFileName().equals(last)) {
//                    ImageView imgPercent = (ImageView)findViewById(R.id.img_resume);
//                    int percent = grid.getPercent();
//                    for (int[] p: GridListAdapter.PERCENT_DRAWABLES) {
//                        if (percent >= p[0]) {
//                            imgPercent.setImageResource(p[1]);
//                            break;
//                        }
//                    }
//                    break;
//                }
//            }
            findViewById(R.id.bt_resume).setOnClickListener(this);
        } else {
            findViewById(R.id.bt_resume).setVisibility(View.GONE);
        }

        // If gridlist is outdated, update and download new grid
        File gridListFile = FileUtils.getGridList();
        long now = Calendar.getInstance().getTimeInMillis();
        long expire = gridListFile.lastModified() + Crossword.GRID_LIST_LIFE_TIME;
        if (gridListFile.exists() == false) {
            //mGridListMessage.setVisibility(View.VISIBLE);
            downloadGrid();
        } else if (now > expire) {
            downloadGrid();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_resume:
                String last = PreferenceManager.getDefaultSharedPreferences(this).getString("last_grid", null);
                if (last != null) {
                    GridModel grid = GridManager.load(last, false);
                    if (grid != null) {
                        Intent intent = new Intent(this, grid.getType() == GridModel.Type.HIDDEN ? GameHiddenActivity.class : GameRegularActivity.class);
                        intent.putExtra("filename", last);
                        startActivity(intent);
                    }
                }
                break;

            case R.id.bt_grids:
                startActivity(new Intent(this, GridListActivity.class));
                break;

            case R.id.bt_word_of_day:
                startActivity(new Intent(this, WordsOfDayActivity.class));
                break;

            case R.id.bt_dictionary:
                startActivity(new Intent(this, DictionaryActivity.class));
                break;
        }
    }

    @Override
    public void onUpdateStart() {
        mLinearUpdate.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUpdateStatus(String status) {
        mLbTextUpdate.setText(status);
    }

    @Override
    public void onUpdateComplete() {
        Animation anim = new AlphaAnimation(1f, 0f);
        anim.setDuration(200);
        anim.setStartOffset(1000);
        anim.setFillAfter(true);
        anim.setFillEnabled(true);
        mLinearUpdate.startAnimation(anim);
    }
}
