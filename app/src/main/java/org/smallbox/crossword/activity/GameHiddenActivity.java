package org.smallbox.crossword.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import org.smallbox.crossword.R;
import org.smallbox.crossword.adapter.GameHiddenGridAdapter;
import org.smallbox.crossword.manager.GridManager;
import org.smallbox.crossword.model.WordModel;

public class GameHiddenActivity extends GameBaseActivity {
    private enum Action {SELECT, CLEAR}

    private int mDownPosX;
    private int mDownPosY;

    private Action mCurrentAction = Action.SELECT;
    private GameHiddenGridAdapter mGridAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_hidden);

        mGridAdapter = new GameHiddenGridAdapter(this, mGrid, mWords, mGridWidth, mGridHeight);
        mGridAdapter.setFont(mGridFont);
        mGridView.setAdapter(mGridAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();

        GameHiddenGridAdapter adapter = (GameHiddenGridAdapter)mGridView.getAdapter();
        if (adapter != null) {
            StringBuilder sb = new StringBuilder();
            for (int x = 0; x < mGridWidth; x++) {
                for (int y = 0; y < mGridHeight; y++) {
                    sb.append(mGridAdapter.getValue(x, y));
                }
            }
            mGrid.setHiddenArea(sb.toString());
        }
        GridManager.writeSave(mGrid, mGridAdapter.getPercent());
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN: {
                mDownPosX = (int)event.getX();
                mDownPosY = (int)event.getY();

                int position = mGridView.pointToPosition(mDownPosX, mDownPosY);

                mDownIsPlayable = true;

                // Stocke les coordonnees d'appuie sur l'ecran
                mTouchPos = position;
                mTouchX = position % mGridWidth;
                mTouchY = position / mGridWidth;
                System.out.println("ACTION_DOWN, x:" + mTouchX + ", y:" + mTouchY + ", position: " + position);

                String value = mGridAdapter.getValue(mTouchX, mTouchY);
                mCurrentAction = Character.isLowerCase(value.charAt(0)) ? Action.SELECT : Action.CLEAR;

                mGridAdapter.notifyDataSetChanged();
            }
            break;

            case MotionEvent.ACTION_MOVE:
                clickOnScreen((int) event.getX(), (int) event.getY());
                break;

            case MotionEvent.ACTION_UP: {
                clickOnScreen((int) event.getX(), (int) event.getY());
                checkGrid();
            }
            break;

            }

        return true;
    }

    private void checkGrid() {
        // Check words
        int nbCorrectWord = 0;
        for (WordModel word: mWords) {
            int x = word.getX();
            int y = word.getY();
            boolean isFullySelected = true;
            for (int i = 0; i < word.getLength(); i++) {
                String value = word.isHorizontal() ? mGridAdapter.getValue(x + i, y) : mGridAdapter.getValue(x, y + i);
                if (Character.isLowerCase(value.charAt(0))) {
                    isFullySelected = false;
                }
            }
            nbCorrectWord += isFullySelected ? 1 : 0;
        }
        Log.e("", "nb word selected: " + nbCorrectWord);

        // Check all areas
        if (mGridAdapter.isCorrect()) {
            Log.e("", "is correct !");
        }
    }

    private void clickOnScreen(int currentX, int currentY) {
        boolean isHorizontal = Math.abs(mDownPosX - currentX) > Math.abs(mDownPosY - currentY);
        int offset = (isHorizontal ? mDownPosX - currentX : mDownPosY - currentY) / mAreaSize;

        int position = mTouchPos;
        actionOnChild(position);
        for (int i = Math.min(0, offset); i < Math.max(0, offset); i++) {
            position += isHorizontal ? 1 : mGridWidth;
            actionOnChild(position);
        }
    }

    private void actionOnChild(int position) {
        int x = position % mGridWidth;
        int y = position / mGridWidth;

        if (x > 0 && y > 0 && x < mGridWidth && y < mGridHeight) {
            System.out.println("ACTION_MOVE, x:" + x + ", y:" + y + ", position: " + position);

            String value = mGridAdapter.getValue(x, y);
            mGridAdapter.setValue(x, y, mCurrentAction == Action.SELECT ? value.toUpperCase() : value.toLowerCase());

            View currentChild = mGridView.getChildAt(position);
            if (currentChild != null) {
                currentChild.setBackgroundResource(mCurrentAction == Action.SELECT ? R.drawable.bg_area_selected: R.drawable.bg_area_white);
                currentChild.setPadding(0, 0, 0, 0);
            }
        }
    }

    @Override
    protected void onPreferencesUpdate(String gridFont, boolean gridIsLower) {
        if (mGridAdapter != null) {
            mGridAdapter.setFont(gridFont);
            mGridAdapter.notifyDataSetChanged();
        }
    }

}
