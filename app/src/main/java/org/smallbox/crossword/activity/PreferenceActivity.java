package org.smallbox.crossword.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.widget.Toast;

import org.smallbox.crossword.R;
import org.smallbox.crossword.utils.FileUtils;

import java.io.File;

public class PreferenceActivity extends android.preference.PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);

        ListPreference listFont = (ListPreference)findPreference("font");
//        listFont.setEntries(new String[] {"default", "almost", "atwriter", "constance", "linowrite", "markers"});
//        listFont.setEntryValues(new String[]{null, "almost", "atwriter", "constance", "linowrite", "markers"});
        listFont.setEntries(new String[] {"default", "almost"});
        listFont.setEntryValues(new String[]{null, "almost"});
        listFont.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                return true;
            }
        });

        findPreference("feedback").setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                startActivity(new Intent(PreferenceActivity.this, FeedbackActivity.class));
                return true;
            }
        });

        findPreference("clear_cache").setOnPreferenceClickListener(new OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				new AlertDialog.Builder(PreferenceActivity.this).setMessage(R.string.preferences_clear_cache_warning)
					.setTitle(R.string.preferences_clear_cache_title)
				    .setCancelable(false)
				    .setPositiveButton(R.string.clear, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            clearCache();
                        }
                    })
				    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    }).create().show();
				return true;
			}
		});

        findPreference("clear_descriptions").setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new AlertDialog.Builder(PreferenceActivity.this).setMessage(R.string.preferences_clear_dictionary_warning)
                        .setTitle(R.string.preferences_clear_dictionary_title)
                        .setCancelable(false)
                        .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                clearDictionary();
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).create().show();
                return true;
            }
        });
	}

    private void clearDictionary() {
        FileUtils.getDictionaryDescription().delete();
        Toast.makeText(this, R.string.preferences_clear_dictionary_toast, Toast.LENGTH_SHORT).show();
        finish();
    }

    protected void clearCache() {
	    for (File file: FileUtils.getGridDirectory().listFiles()) {
            file.delete();
        }

	    File gridList = FileUtils.getGridList();
	    if (gridList.exists()) {
            gridList.delete();
        }

		Toast.makeText(this, R.string.preferences_clear_cache_toast, Toast.LENGTH_SHORT).show();
		finish();
	}

}
