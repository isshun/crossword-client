package org.smallbox.crossword.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.GridView;
import android.widget.Toast;

import org.smallbox.crossword.Crossword;
import org.smallbox.crossword.R;
import org.smallbox.crossword.manager.GridManager;
import org.smallbox.crossword.model.GridModel;
import org.smallbox.crossword.model.WordModel;

import java.util.List;

/**
 * Created by Alex on 06/02/2015.
 */
public abstract class GameBaseActivity extends BaseActivity implements View.OnTouchListener {
    public static final java.lang.String EXTRA_FILENAME = "filename";

    public enum Mode {NORMAL, CHECK, SOLVE};
    public Mode mGridMode = Mode.NORMAL;

    protected GridView          mGridView;

    protected GridModel         mGrid;
    protected List<WordModel>   mWords;		// Liste des mots

    protected boolean           mDownIsPlayable;	// false si le joueur à appuyé sur une case noire
    protected int               mTouchPos;		// Position ou le joueur à appuyé
    protected int               mTouchX;			// Ligne ou le joueur à appuyé
    protected int               mTouchY;			// Colonne ou le joueur à appuyé
    protected int               mCurrentPos;		// Position actuelle du curseur
    protected int               mCurrentX;		// Colonne actuelle du curseur
    protected int               mCurrentY;		// Ligne actuelle du curseur
    protected boolean           mSelectionIsHorizontal;		// Sens de la selection

    protected boolean           mSolidSelection;	// PREFERENCES: Selection persistante
    protected boolean           mGridIsLower;	// PREFERENCES: Grille en minuscule
    protected String            mGridFont;

    protected int               mGridWidth;
    protected int               mGridHeight;
    protected int               mAreaSize;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGrid = GridManager.load(getIntent().getExtras().getString(EXTRA_FILENAME), true);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        readPreferences();

        mWords = mGrid.getWords();
        setTitle(mGrid.getName());

        mGridWidth = mGrid.getWidth();
        mGridHeight = mGrid.getHeight();
        mAreaSize = Crossword.getScreenWidth() / mGridWidth;

        mGridView = (GridView)findViewById(R.id.grid);
        mGridView.setOnTouchListener(this);
        mGridView.setNumColumns(mGridWidth);
        mGridView.setVerticalScrollBarEnabled(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Crossword.REQUEST_PREFERENCES:
                if (Crossword.DEBUG) Toast.makeText(this, "PREFERENCES_UPDATED", Toast.LENGTH_SHORT).show();
                readPreferences();
                onPreferencesUpdate(mGridFont, mGridIsLower);
                break;
        }
    }

    protected abstract void onPreferencesUpdate(String gridFont, boolean gridIsLower);

    private void readPreferences() {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        mSolidSelection = preferences.getBoolean("solid_selection", false);
        mGridIsLower = preferences.getBoolean("grid_is_lower", false);
        mGridFont = preferences.getString("font", null);
        if (mGridMode != Mode.SOLVE) {
            mGridMode = preferences.getBoolean("grid_check", false) ? Mode.CHECK : Mode.NORMAL;
        }
    }

}
