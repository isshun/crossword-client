package org.smallbox.crossword.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.smallbox.crossword.R;
import org.smallbox.crossword.activity.GameBaseActivity;
import org.smallbox.crossword.activity.GameHiddenActivity;
import org.smallbox.crossword.activity.GameRegularActivity;
import org.smallbox.crossword.activity.GridInfoActivity;
import org.smallbox.crossword.adapter.GridListAdapter;
import org.smallbox.crossword.manager.GridManager;
import org.smallbox.crossword.model.GridModel;

import java.util.List;

/**
 * Created by Alex on 06/02/2015.
 */
public class GridListFragment extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    private static final String ARG_TYPE_NAME = "type_name";

    private ListView            mGridList;
    private TextView            mGridListMessage;
    private GridListAdapter     mGridAdapter;

    public static Fragment newInstance(GridModel.Type type) {
        Fragment fragment = new GridListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TYPE_NAME, type.name());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_grid_list, container, false);

        mGridList = (ListView)view.findViewById(org.smallbox.crossword.R.id.gridListView);
        mGridList.setOnItemClickListener(this);
        mGridList.setOnItemLongClickListener(this);
        mGridListMessage = (TextView)view.findViewById(org.smallbox.crossword.R.id.gridListMessage);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        readGridDirectory();
    }

    private void readGridDirectory() {
        List<GridModel> grids = GridManager.loadList();
        if (grids != null) {
            // Display grids
            mGridAdapter = new GridListAdapter(grids, GridModel.Type.valueOf(getArguments().getString(ARG_TYPE_NAME)));
            mGridList.setAdapter(mGridAdapter);

            // Hide message
            mGridListMessage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        GridModel grid = (GridModel)mGridAdapter.getItem(position);

        // Save grid name in preference (from 'last grid' on main menu)
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        preferences.edit().putString("last_grid", grid.getFileName()).commit();

        // Launch grid
        Intent intent = new Intent(getActivity(), grid.getType() == GridModel.Type.HIDDEN ? GameHiddenActivity.class : GameRegularActivity.class);
        intent.putExtra(GameBaseActivity.EXTRA_FILENAME, grid.getFileName());
        startActivity(intent);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        GridModel grid = (GridModel)this.mGridAdapter.getItem(position);
        Intent intent = new Intent(getActivity(), GridInfoActivity.class);
        intent.putExtra(GridInfoActivity.EXTRA_FILE_NAME, grid.getFileName());
        startActivity(intent);
        return true;
    }
}
