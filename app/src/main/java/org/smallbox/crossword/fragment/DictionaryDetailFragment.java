package org.smallbox.crossword.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.smallbox.crossword.R;
import org.smallbox.crossword.model.DictionaryEntryModel;
import org.smallbox.crossword.utils.FileUtils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Alex on 13/02/2015.
 */
public class DictionaryDetailFragment extends Fragment {
    public static final String ARG_WORD = "arg_word";
    public static final String ARG_DESCRIPTION_START = "arg_description_start";
    public static final String ARG_DESCRIPTION_END = "arg_description_end";
    private AsyncTask<String, String, String> mTask;

    public static Fragment newInstance(DictionaryEntryModel entry) {
        Fragment fragment = new DictionaryDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_WORD, entry.word);
        args.putInt(ARG_DESCRIPTION_START, entry.start);
        args.putInt(ARG_DESCRIPTION_END, entry.end);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dictionary_detail, container, false);
        final Handler handler = new Handler();

                ((TextView) view.findViewById(R.id.lb_word)).setText(getArguments().getString(ARG_WORD));

        mTask = new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    int start = getArguments().getInt(ARG_DESCRIPTION_START, -1);
                    int end = getArguments().getInt(ARG_DESCRIPTION_END, -1);
                    BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(FileUtils.getDictionaryDescription()), "UTF-8"));
                    if (!isCancelled()) {
                        final char[] buff = new char[end - start];
                        br.skip(start);
                        br.read(buff, 0, end - start);
                        br.close();
                        if (!isCancelled()) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    ((TextView) view.findViewById(R.id.lb_description)).setText(String.valueOf(buff).replace("\\n", "\n\n"));
                                }
                            });
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
        mTask.execute();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mTask.cancel(true);
    }

}
