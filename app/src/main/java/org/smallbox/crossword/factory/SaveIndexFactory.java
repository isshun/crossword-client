package org.smallbox.crossword.factory;

import java.util.Map;

/**
 * Created by Alex on 10/02/2015.
 */
public class SaveIndexFactory {
    public static String toSaveIndex(Map<String, Integer> saveIndex) {
        StringBuilder sb = new StringBuilder();

        sb.append("<saves>");
        for (Map.Entry<String, Integer> save: saveIndex.entrySet()) {
            sb.append("<save file=\"").append(save.getKey()).append("\">").append(save.getValue()).append("</save>");
        }
        sb.append("</saves>");

        return sb.toString();
    }
}
