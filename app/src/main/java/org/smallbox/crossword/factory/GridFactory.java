package org.smallbox.crossword.factory;

import org.smallbox.crossword.model.GridModel;
import org.smallbox.crossword.model.WordModel;

/**
 * Created by Alex on 05/02/2015.
 */
public class GridFactory {
    public static String toSaveXML(GridModel grid) {
        StringBuffer wordHorizontal = new StringBuffer();
        StringBuffer wordVertical = new StringBuffer();
        for (WordModel word: grid.getWords()) {
            int x = word.getX();
            int y = word.getY();
            String value = String.format(
                    "<word x=\"%d\" y=\"%d\">%s</word>\n",
                    x,
                    y,
                    word.getTmp() != null ? word.getTmp() : "");
            if (word.isHorizontal()) {
                wordHorizontal.append(value);
            } else {
                wordVertical.append(value);
            }
        }

        StringBuffer sb = new StringBuffer();
        sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n")
                .append("<grid>\n")
                .append("<horizontal>\n")
                .append(wordHorizontal)
                .append("</horizontal>\n")
                .append("<vertical>\n")
                .append(wordVertical)
                .append("</vertical>\n")
                .append("</grid>\n");

        return sb.toString();
    }

    public static String toXML(GridModel grid) {
        StringBuffer wordHorizontal = new StringBuffer();
        StringBuffer wordVertical = new StringBuffer();
        for (WordModel word: grid.getWords()) {
            int x = word.getX();
            int y = word.getY();
            String value = String.format(
                    "<word x=\"%d\" y=\"%d\" description=\"%s\" tmp=\"%s\" placeholder=\"%s\">%s</word>\n",
                    x,
                    y,
                    word.getDescription(),
                    word.getTmp() != null ? word.getTmp() : "",
                    word.getPlaceholder() != null ? word.getPlaceholder() : "",
                    word.getText());
            if (word.isHorizontal())
                wordHorizontal.append(value);
            else
                wordVertical.append(value);
        }

        StringBuffer sb = new StringBuffer();
        sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n")
                .append("<grid>\n")
                .append("<name>").append(grid.getName()).append("</name>\n")
                .append("<description>").append(grid.getDescription()).append("</description>\n")
                .append(grid.getRawDate() != null ? "<date>" + grid.getRawDate() + "</date>\n" : "")
                .append(grid.getType() == GridModel.Type.HIDDEN ? "<hiddenArea>" + grid.getHiddenArea() + "</hiddenArea>\n" : "")
                .append("<author>").append(grid.getAuthor()).append("</author>\n")
                .append("<level>").append(grid.getLevel()).append("</level>\n")
                .append("<type>").append(grid.getType().name()).append("</type>\n")
                .append("<percent>0</percent>\n")
                .append("<width>").append(grid.getWidth()).append("</width>\n")
                .append("<height>").append(grid.getHeight()).append("</height>\n")
                .append("<horizontal>\n")
                .append(wordHorizontal)
                .append("</horizontal>\n")
                .append("<vertical>\n")
                .append(wordVertical)
                .append("</vertical>\n")
                .append("</grid>\n");

        return sb.toString();
    }
}
