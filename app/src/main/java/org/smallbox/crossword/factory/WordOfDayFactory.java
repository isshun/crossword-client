package org.smallbox.crossword.factory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.smallbox.crossword.Crossword;
import org.smallbox.crossword.model.WordModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Alex on 05/02/2015.
 */
public class WordOfDayFactory {
    public static List<WordModel> load(String json) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(Crossword.LAUNCH_DATE);

        try {
            List<WordModel> words = new ArrayList<>();

            JSONArray array = new JSONArray(json);
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);

                WordModel word = new WordModel();
                word.setDate(calendar.getTime());
                word.setText(obj.getString("word"));
                word.setDescription(obj.getString("define"));
                word.setQuote(obj.getString("quote"));
                words.add(word);

                calendar.add(Calendar.DAY_OF_MONTH, 1);
            }

            return words;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
