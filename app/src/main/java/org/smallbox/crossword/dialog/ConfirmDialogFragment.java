package org.smallbox.crossword.dialog;

import android.text.Html;
import android.util.TypedValue;
import android.widget.TextView;

import org.smallbox.lib.dialog.BaseDialogFragment;
import org.smallbox.lib.utils.Utils;

/**
 * Created by Alex on 12/02/2015.
 */
public abstract class ConfirmDialogFragment extends BaseDialogFragment {
    private static final int PADDING_TOP = Utils.convertDpToPixel(7);
    private static final int PADDING = Utils.convertDpToPixel(10);

    public static ConfirmDialogFragment newInstance(final int resIdTitle, final int resIdContent, final int resIdConfirm, final int resIdCancel) {
        return new ConfirmDialogFragment() {
            @Override
            protected void onCreateDialog() {
                setTitle(resIdTitle);
                setNegativeButton(resIdCancel);
                setPositiveButton(resIdConfirm);

                TextView lbContent = new TextView(getActivity());
                lbContent.setPadding(PADDING, PADDING_TOP, PADDING, PADDING);
                lbContent.setTextColor(0xff333333);
                lbContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                lbContent.setText(Html.fromHtml(getString(resIdContent)));
                setContentView(lbContent);
            }

            @Override
            protected void onCancel() {
            }

            @Override
            protected void onConfirm() {
            }
        };
    }
}
