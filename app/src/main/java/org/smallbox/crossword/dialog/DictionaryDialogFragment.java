package org.smallbox.crossword.dialog;

import org.smallbox.crossword.R;
import org.smallbox.lib.dialog.BaseDialogFragment;

/**
 * Created by Alex on 12/02/2015.
 */
public abstract class DictionaryDialogFragment extends BaseDialogFragment {
    public static ConfirmDialogFragment newInstance() {
        return new ConfirmDialogFragment() {
            @Override
            protected void onCreateDialog() {
                setTitle(R.string.dialog_download_dictionary_title);
                setNegativeButton(R.string.lb_close);
                setPositiveButton(R.string.lb_download);
                setContentView(R.layout.dialog_dictionary);
            }

            @Override
            protected void onCancel() {
            }

            @Override
            protected void onConfirm() {
            }
        };
    }
}

