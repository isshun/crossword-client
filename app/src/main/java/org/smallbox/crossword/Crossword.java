/*
 * Copyright 2011 Alexis Lauper <alexis.lauper@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.smallbox.crossword;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.graphics.Point;
import android.preference.PreferenceManager;
import android.view.WindowManager;

import org.smallbox.crossword.utils.FileUtils;
import org.smallbox.crossword.view.CoolTextView;
import org.smallbox.lib.SmallboxLib;

import java.util.Calendar;
import java.util.Date;

public class Crossword extends Application {
    public static final String 	DICTIONARY_FULL_URL = "http://crossword.smallbox.org/site_media/dico_full.zip";
    public static final String 	DICTIONARY_URL = "http://crossword.smallbox.org/site_media/dico.zip";

    public static final String 	GRID_URL = "http://crossword.smallbox.org/grids/%s";
    public static final String  GRID_LIST_URL = "http://crossword.smallbox.org/grids.xml";
    public static final long    GRID_LIST_LIFE_TIME = 86400000;

    public static final String 	MAIL_URL = "http://crossword.smallbox.org/mail.html";
    public static final String 	FEEDBACK_URL = "http://crossword.smallbox.org/feedback.html";
    public static final int		REQUEST_PREFERENCES = 2;
    public static final float 	KEYBOARD_OVERLAY_OFFSET = 90;
    public static final int 	NOTIFICATION_DOWNLOAD_ID = 1;
    public static final String  LOG_TAG = "Crossword";

    public static final Date LAUNCH_DATE;
    static {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2015, Calendar.FEBRUARY, 13, 0, 0, 0);
        LAUNCH_DATE = calendar.getTime();
    }

    public static boolean 		DEBUG;

    private static Context sContext;
    private static WindowManager sWindowManager;

    public void onCreate() {
        sContext = getApplicationContext();
        sWindowManager = (WindowManager)sContext.getSystemService(Context.WINDOW_SERVICE);

        Crossword.DEBUG = (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;

        SmallboxLib.setContext(sContext);
        SmallboxLib.addFont(CoolTextView.Font.FUTURA_BOLD.ordinal(), "futura_bold", "fonts/Quicksand_Bold.otf");
        SmallboxLib.addFont(CoolTextView.Font.FUTURA_THIN.ordinal(), "futura_thin", "fonts/Quicksand_Light.otf");
        SmallboxLib.addFont(CoolTextView.Font.FUTURA.ordinal(), "futura", "fonts/Quicksand_Book.otf");
        SmallboxLib.addFont(CoolTextView.Font.ALMOST.ordinal(), "almost", "fonts/almost.ttf");
//        SmallboxLib.addFont(CoolTextView.Font.ATWRITER.ordinal(), "atwriter", "fonts/atwriter.ttf");
//        SmallboxLib.addFont(CoolTextView.Font.CHELTENHAM.ordinal(), "cheltenham", "fonts/cheltenham.ttf");
//        SmallboxLib.addFont(CoolTextView.Font.CONSTANCE.ordinal(), "constance", "fonts/constance.ttf");
//        SmallboxLib.addFont(CoolTextView.Font.LINOWRITE.ordinal(), "linowrite", "fonts/linowrite.ttf");
//        SmallboxLib.addFont(CoolTextView.Font.MARKERS.ordinal(), "markers", "fonts/markers.ttf");
        FileUtils.createDirectories();

        // Create default preference on first launch
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (preferences.getBoolean("first_launch", true)) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("first_launch", false);
            editor.putBoolean("grid_is_lower", true);
            editor.putString("font", "almost");
            editor.apply();
        }
    }

    public static int getScreenWidth() {
        Point size = new Point();
        sWindowManager.getDefaultDisplay().getSize(size);
        return size.x;
    }

    public static int getScreenHeight() {
        Point size = new Point();
        sWindowManager.getDefaultDisplay().getSize(size);
        return size.y;
    }

    public static Context getContext() {
        return sContext;
    }

    public static int getColor(int resId) {
        return sContext.getResources().getColor(resId);
    }
}