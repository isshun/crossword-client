package org.smallbox.crossword.rest;

import org.smallbox.crossword.Crossword;
import org.smallbox.crossword.CrosswordException;
import org.smallbox.crossword.manager.GridManager;
import org.smallbox.crossword.model.GridModel;
import org.smallbox.crossword.utils.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadManager {
    public static interface DownloadListener {
        void onBegin(HttpURLConnection connection);
        void onProgress(int downloadedLength, int contentLength);
        void onComplete(int contentLength);
        void onFailure();
    }

    public static void download(String in, File out) throws CrosswordException {
        download(in, out, null);
    }

	public static void download(String in, File out, DownloadListener listener) throws CrosswordException {
		InputStream input = null;
	    FileOutputStream writeFile = null;

        HttpURLConnection connection = null;
	    try {
	        URL url = new URL(in);
	        System.out.println("Download file: " + url.toString());
            connection = (HttpURLConnection)url.openConnection();
            if (listener != null) {
                listener.onBegin(connection);
            }
            int contentLength = connection.getContentLength();
            int downloadedLength = 0;
	        input = connection.getInputStream();
	        writeFile = new FileOutputStream(out);
	        int read;
	        byte[] buffer = new byte[1024];
	        while ((read = input.read(buffer)) > 0) {
                writeFile.write(buffer, 0, read);
                downloadedLength += read;
                if (listener != null) {
                    listener.onProgress(downloadedLength, contentLength);
                }
            }
	        writeFile.flush();
            if (listener != null) {
                listener.onComplete(contentLength);
            }
	    } catch (IOException e) {
	        System.out.println("Error while trying to download the file.");
	        e.printStackTrace();
            if (listener != null) {
                listener.onFailure();
            }
	    } finally {
	        try {
	        	if (writeFile != null)
	        		writeFile.close();
	        	if (input != null)
	        		input.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
            connection.disconnect();
	    }
	}
	
	public static void downloadGrid(String filename) throws CrosswordException {
        DownloadManager.download(String.format(Crossword.GRID_URL, filename), FileUtils.getGrid(filename));

        GridModel grid = GridManager.load(filename, true);
        if (grid != null && (grid.getType() == GridModel.Type.SLAM || grid.getType() == GridModel.Type.HIDDEN)) {
            grid.init();
            GridManager.writeGrid(grid);
        }
	}

	public static void downloadListGrid() throws CrosswordException {
		DownloadManager.download(Crossword.GRID_LIST_URL, FileUtils.getGridList());
	}
}
