package org.smallbox.crossword.rest;

import android.content.Context;
import android.os.AsyncTask;

import org.smallbox.crossword.CrosswordException;
import org.smallbox.crossword.GridIndexCompare;
import org.smallbox.crossword.R;
import org.smallbox.crossword.model.GridIndexModel;
import org.smallbox.crossword.parser.GridListParser;
import org.smallbox.crossword.parser.SAXFileHandler;
import org.smallbox.crossword.utils.FileUtils;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

public class UpdateGridsTask extends AsyncTask<String, Integer, String> {
    private boolean                 mIsSucceed = false;
    private String                  mErrorMessage = "";
	private HttpURLConnection       mUrlConnection;
	private AsyncTaskListener mListener;
	private Context                 mContext;
	private boolean                 mCompleted;
	private String                  mStatus;
	private int                     mProgress;

	public UpdateGridsTask(Context context, AsyncTaskListener listener) {
		mListener = listener;
		mContext = context;
		mStatus = context.getResources().getString(R.string.notification_update_grids_status);
	}
        
	protected void onPreExecute() {
		super.onPreExecute();
    }
        
	protected String doInBackground(String... params) {
		try {
			// Get and parse grid index
			DownloadManager.downloadListGrid();
            GridIndexModel newIndex = (GridIndexModel) SAXFileHandler.read(GridListParser.class, FileUtils.getGridList());

            // Check new grids
            final List<String> newGrids = new ArrayList<>();
            GridIndexCompare.getNewGrids(newIndex, new GridIndexCompare.GridIndexCompareListener() {
                @Override
                public void onGridChange(String filename) {
                    newGrids.add(filename);
                }
            });

            // Download new grids
            mListener.onTaskStarted();
            for (String filename: newGrids) {
                DownloadManager.downloadGrid(filename);
                publishProgress(++mProgress, newGrids.size());
            }
		} catch (CrosswordException e) {
			e.printStackTrace();
			mErrorMessage = e.getMessage();
			mIsSucceed = false;
			return null;
		}
        mIsSucceed = true;
		return null;
	}

    protected void onProgressUpdate(Integer... args) {
    	mListener.onTaskUpdate(String.format(mStatus, args[0], args[1]), args[0], args[1]);
    }
        
    public void setActivity(AsyncTaskListener delegate) {
    	mListener = delegate;
    	if (mCompleted) {
    		notifyActivityTaskCompleted();
    	}
    }

    protected void onCancelled() {
    	mUrlConnection.disconnect();
    }

    protected void onPostExecute(String unused) {
    	mCompleted = true;
    	notifyActivityTaskCompleted();
    }
    
    private void notifyActivityTaskCompleted() {
    	if (mListener == null)
    		return;
    	
    	mListener.onTaskComplete(mIsSucceed, mErrorMessage);
    }
}
