package org.smallbox.crossword.rest;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import org.smallbox.crossword.Crossword;
import org.smallbox.crossword.CrosswordException;
import org.smallbox.crossword.utils.FileUtils;
import org.smallbox.lib.utils.ZipUtils;

import java.io.File;
import java.net.HttpURLConnection;

public class GetDictionaryTask extends AsyncTask<Boolean, Integer, String> {
    private final Handler               mHandler;
	private AsyncTaskListener           mListener;
    private HttpURLConnection           mUrlConnection;

	public GetDictionaryTask(Context context) {
        mHandler = new Handler(Looper.getMainLooper());
	}
        
	protected String doInBackground(Boolean... params) {
		try {
			// Get zip
            File zipFile = new File(FileUtils.getDirectory(), "tmp.zip");
            DownloadManager.download(params[0] ? Crossword.DICTIONARY_FULL_URL : Crossword.DICTIONARY_URL, zipFile, new DownloadManager.DownloadListener() {
                @Override
                public void onBegin(HttpURLConnection connection) {
                    mUrlConnection = connection;
                }

                @Override
                public void onProgress(int downloadedLength, int contentLength) {
                    onPostUpdate("Téléchargement...", downloadedLength, contentLength);
                }

                @Override
                public void onComplete(int contentLength) {
                }

                @Override
                public void onFailure() {
                }
            });

            // Unzip
            onPostUpdate("Décompréssion...", -1, -1);
            ZipUtils.unpackZip(FileUtils.getDirectory(), zipFile);

            // Delete zip file
            zipFile.delete();
        } catch (CrosswordException e) {
			e.printStackTrace();
			return e.getMessage();
		}

		return "Unknown error";
	}

    private void onPostUpdate(final String message, final int progress, final int total) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mListener != null) {
                    mListener.onTaskUpdate(message, progress, total);
                }
            }
        });
    }

    @Override
    protected void onCancelled() {
    	mUrlConnection.disconnect();
    }

    @Override
    protected void onPostExecute(final String errorMessage) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mListener != null) {
                    mListener.onTaskComplete(errorMessage == null, errorMessage);
                }
            }
        });
    }

    public void setListener(AsyncTaskListener listener) {
        mListener = listener;
    }

    public void removeListener() {
        mListener = null;
    }
}
