package org.smallbox.crossword.rest;

public interface AsyncTaskListener {
    public void onTaskStarted();
    public void onTaskUpdate(String status, int progress, int total);
    public void onTaskComplete(boolean completed, String errorMessage);
}
