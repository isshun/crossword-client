package org.smallbox.crossword;

import android.view.animation.Animation;

/**
 * Created by Alex on 05/02/2015.
 */
public abstract class OnAnimationEndListener implements Animation.AnimationListener {
    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }
}
