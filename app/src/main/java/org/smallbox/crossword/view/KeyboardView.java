/*
 * Copyright 2011 Alexis Lauper <alexis.lauper@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.smallbox.crossword.view;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.smallbox.crossword.OnAnimationEndListener;
import org.smallbox.crossword.R;
import org.smallbox.lib.utils.Utils;

public class KeyboardView extends LinearLayout implements OnTouchListener {
    private static int OVERLAY_SIZE;

    public interface KeyboardListener {
        void onKey(String value);
        void onDraftChange(boolean isDraft);
    }

    private interface OnKeyListener {
        void onKeyDown(View view, String value, int location[], int width);
        void onKeyUp(View view, String value, int location[], int width);
    }

    private KeyboardListener    mKeyboardListener;
    private OnKeyListener       mKeyListener;
    private FrameLayout         mKeyboardOverlay;
    private View                mCurrentView;
    private String              mValue;
    private boolean             mIsDraft;
    private ValueAnimator       mColorAnimation;
    private View                mBtDeleteActive;

    public KeyboardView(Context context) {
        super(context);
        this.initComponent();
    }

    public KeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initComponent();
    }

    public void initComponent() {
        if (!isInEditMode()) {
            OVERLAY_SIZE = Utils.convertDpToPixel(36);

            LayoutInflater.from(getContext()).inflate(R.layout.keyboard, this);

            mKeyboardOverlay = (FrameLayout) findViewById(R.id.keyboard_overlay);

            this.findViewById(R.id.buttonA).setOnTouchListener(this);
            this.findViewById(R.id.buttonB).setOnTouchListener(this);
            this.findViewById(R.id.buttonC).setOnTouchListener(this);
            this.findViewById(R.id.buttonD).setOnTouchListener(this);
            this.findViewById(R.id.buttonE).setOnTouchListener(this);
            this.findViewById(R.id.buttonF).setOnTouchListener(this);
            this.findViewById(R.id.buttonG).setOnTouchListener(this);
            this.findViewById(R.id.buttonH).setOnTouchListener(this);
            this.findViewById(R.id.buttonI).setOnTouchListener(this);
            this.findViewById(R.id.buttonJ).setOnTouchListener(this);
            this.findViewById(R.id.buttonK).setOnTouchListener(this);
            this.findViewById(R.id.buttonL).setOnTouchListener(this);
            this.findViewById(R.id.buttonM).setOnTouchListener(this);
            this.findViewById(R.id.buttonN).setOnTouchListener(this);
            this.findViewById(R.id.buttonO).setOnTouchListener(this);
            this.findViewById(R.id.buttonP).setOnTouchListener(this);
            this.findViewById(R.id.buttonQ).setOnTouchListener(this);
            this.findViewById(R.id.buttonR).setOnTouchListener(this);
            this.findViewById(R.id.buttonS).setOnTouchListener(this);
            this.findViewById(R.id.buttonT).setOnTouchListener(this);
            this.findViewById(R.id.buttonU).setOnTouchListener(this);
            this.findViewById(R.id.buttonV).setOnTouchListener(this);
            this.findViewById(R.id.buttonW).setOnTouchListener(this);
            this.findViewById(R.id.buttonX).setOnTouchListener(this);
            this.findViewById(R.id.buttonY).setOnTouchListener(this);
            this.findViewById(R.id.buttonZ).setOnTouchListener(this);
            this.findViewById(R.id.buttonDELETE).setOnTouchListener(this);

            mBtDeleteActive = findViewById(R.id.buttonDELETEActive);

            mKeyListener = new OnKeyListener() {
                @Override
                public void onKeyDown(View view, String value, int location[], int width) {
                }

                @Override
                public void onKeyUp(View view, String value, int location[], int width) {
                    int offsetX = (OVERLAY_SIZE - width) / 2;
                    addOverlay(view, location[0] - offsetX, location[1] + Utils.convertDpToPixel(4));

                    if (mKeyboardListener != null) {
                        mKeyboardListener.onKey(value);
                    }
                }
            };
        }
    }

    private void addOverlay(final View view, int x, int y) {
        final ImageView imgOverlay = new ImageView(getContext());
        imgOverlay.setImageResource(R.drawable.keyboard_selector);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(OVERLAY_SIZE, OVERLAY_SIZE);
        lp.leftMargin = x;
        lp.topMargin = y;
        mKeyboardOverlay.addView(imgOverlay, lp);

        if (view instanceof TextView) {
            final TextView textView = (TextView)view;
            Integer colorFrom = 0xffffffff;
            Integer colorTo = 0xff333333;
            mColorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
            mColorAnimation.setDuration(200);
            mColorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    textView.setTextColor((Integer) animator.getAnimatedValue());
                }

            });
            mColorAnimation.start();
        } else if (view instanceof FrameLayout) {
            Animation anim = new AlphaAnimation(0f, 1f);
            anim.setDuration(200);
            mBtDeleteActive.startAnimation(anim);
            mBtDeleteActive.setVisibility(VISIBLE);
        }

        int size = Utils.convertDpToPixel(36);
        final ScaleAnimation anim = new ScaleAnimation(0f, 1f, 0f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.initialize(size, size, 0, 0);
        anim.setDuration(200);
        anim.setInterpolator(getContext(), android.R.interpolator.decelerate_cubic);
        anim.setAnimationListener(new OnAnimationEndListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                if (view instanceof TextView) {
                    final TextView textView = (TextView)view;
                    Integer colorFrom = 0xff333333;
                    Integer colorTo = 0xffffffff;
                    ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
                    colorAnimation.setDuration(200);
                    colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator animator) {
                            textView.setTextColor((Integer)animator.getAnimatedValue());
                        }

                    });
                    colorAnimation.start();
                } else if (view instanceof FrameLayout) {
                    Animation anim = new AlphaAnimation(1f, 0f);
                    anim.setDuration(200);
                    mBtDeleteActive.startAnimation(anim);
                    mBtDeleteActive.setVisibility(INVISIBLE);
                }

                Animation fadeOff = new AlphaAnimation(1f, 0f);
                fadeOff.setDuration(200);
                fadeOff.setAnimationListener(new OnAnimationEndListener() {
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mKeyboardOverlay.removeView(imgOverlay);
                    }
                });
                imgOverlay.startAnimation(fadeOff);
            }
        });
        imgOverlay.startAnimation(anim);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                // Get key mValue
                switch (v.getId()) {
                    case R.id.buttonA: this.mValue = "A"; break;
                    case R.id.buttonB: this.mValue = "B"; break;
                    case R.id.buttonC: this.mValue = "C"; break;
                    case R.id.buttonD: this.mValue = "D"; break;
                    case R.id.buttonE: this.mValue = "E"; break;
                    case R.id.buttonF: this.mValue = "F"; break;
                    case R.id.buttonG: this.mValue = "G"; break;
                    case R.id.buttonH: this.mValue = "H"; break;
                    case R.id.buttonI: this.mValue = "I"; break;
                    case R.id.buttonJ: this.mValue = "J"; break;
                    case R.id.buttonK: this.mValue = "K"; break;
                    case R.id.buttonL: this.mValue = "L"; break;
                    case R.id.buttonM: this.mValue = "M"; break;
                    case R.id.buttonN: this.mValue = "N"; break;
                    case R.id.buttonO: this.mValue = "O"; break;
                    case R.id.buttonP: this.mValue = "P"; break;
                    case R.id.buttonQ: this.mValue = "Q"; break;
                    case R.id.buttonR: this.mValue = "R"; break;
                    case R.id.buttonS: this.mValue = "S"; break;
                    case R.id.buttonT: this.mValue = "T"; break;
                    case R.id.buttonU: this.mValue = "U"; break;
                    case R.id.buttonV: this.mValue = "V"; break;
                    case R.id.buttonW: this.mValue = "W"; break;
                    case R.id.buttonX: this.mValue = "X"; break;
                    case R.id.buttonY: this.mValue = "Y"; break;
                    case R.id.buttonZ: this.mValue = "Z"; break;
                    case R.id.buttonDELETE: this.mValue = null; break;
                }

                this.mCurrentView = v;

                int[] location = new int[2];
                location[0] = (int) v.getX();
                location[1] = (int) ((View)v.getParent()).getY();
                mKeyListener.onKeyDown(v, mValue, location, this.mCurrentView.getWidth());

                break;
            }

            case MotionEvent.ACTION_UP:
            {
                int[] location = new int[2];
                location[0] = (int) v.getX();
                location[1] = (int)((View)v.getParent()).getY();

                switch (v.getId()) {

                    case R.id.buttonDELETE:
                        mKeyListener.onKeyUp(v, " ", location, mCurrentView.getWidth());
                        break;

                    default:
                        mKeyListener.onKeyUp(v, mValue, location, mCurrentView.getWidth());
                        break;
                }

                v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);

                break;
            }
        }

        return true;
    }

    public void setKeyboardListener(KeyboardListener keyboardListener) {
        mKeyboardListener = keyboardListener;
    }
}