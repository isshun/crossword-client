package org.smallbox.crossword.view;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by Alex on 11/02/2015.
 */
public abstract class OnTextChangeListener implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}
