package org.smallbox.crossword.view;

import android.content.Context;
import android.util.AttributeSet;

public class CoolTextView extends org.smallbox.lib.view.CoolTextView {
    public enum Font {ALMOST, ATWRITER, CONSTANCE, LINOWRITE, MARKERS, CHELTENHAM, FUTURA, FUTURA_THIN, FUTURA_BOLD}

    public CoolTextView(Context context) {
        super(context);
    }

    public CoolTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CoolTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setFont(Font font) {
        setFontIndex(font.ordinal());
    }
}
